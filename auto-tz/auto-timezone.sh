#!/bin/bash
#if [ $(curl -sL -w "%{http_code}" "https://ipapi.co/timezone" -o /dev/null) == "200" ]; then
#  timedatectl set-timezone $(curl https://ipapi.co/timezone -s)
#fi
## second type
if [ $(curl -sL -w "%{http_code}" "http://ip-api.com" -o /dev/null) == "200" ]; then
  IPADDR=$(curl -s http://ipv4bot.whatismyipaddress.com)
  TZTEXT=$(curl -s http://ip-api.com/json/${IPADDR}?fields=timezone)
  TZ=$(echo "${TZTEXT//[\"\}]}" | awk -F':' '{print $2}')
  timedatectl set-timezone $TZ
fi
return 0
