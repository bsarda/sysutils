# Automatic Timezone

This is a small bash script to set the timezone.

## copy

```shell
sudo cp auto-timezone.sh /opt/
sudo chown ${USER:=$(/usr/bin/id -run)}:$USER auto-timezone.sh
*/5 * * * * /home/ubuntu/backup.sh
```

## Launch at start

Add to the cron, every 30 minutes here

```shell
sudo crontab -e
*/30 * * * * /home/ubuntu/backup.sh
```

## Check cron

```shell
sudo crontab -l
sudo grep CRON /var/log/syslog
```
