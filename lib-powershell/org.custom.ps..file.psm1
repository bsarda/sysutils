# ----------------------------------------------------------------------------------------------------
function Test-FileLock{
	<#
	.SYNOPSIS
		test de verrou sur un fichier
	.DESCRIPTION
		test de verrou sur un fichier
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Path
		nom du fichier (chemin complet)
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[String] Path
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[Int] ErrorCode
		ERRORCODES:
			1 : exécution OK, fichier libre
			0 : exécution OK, fichier locké
			-1: erreur des arguments, mauvais format
			-2: fichier innexistant
			-3: exception lors du fileinfo
	.EXAMPLE
		Test-FileLock -Path "c:\temp.csv"
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Path",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(Mandatory=$true,Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$Path,
		[Parameter(Mandatory=$false)]
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)

	if( ($Path -isnot [String]) -or ([String]::IsNullOrEmpty($Path)) ){
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect parameters"
		}
		return -1
	}

	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [PROGRESS] The function '$($MyInvocation.InvocationName)' is running"
	}

	# test d'existence du fichier
	if((Test-Path -Path $Path) -eq $false){
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [WARNING] The file '$Path' does not exists"
		}
    	return -2
  	}      
	# création de l'objet fichier
	$o_File = New-Object System.IO.FileInfo $Path
	if( !$o_File ){
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Cant create file pointer"
		}
    	return -3
	}
  	try
  	{
		# ouverture du stream
		$fs_RWTest = $o_File.Open([System.IO.FileMode]::Open, [System.IO.FileAccess]::ReadWrite, [System.IO.FileShare]::None)
      	if ($fs_RWTest){
        	$fs_RWTest.Close()
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] File '$Path' is accessible, R/W mode"
			}
      		return 1
      	}
		else{
			$fs_ROTest = $o_File.Open([System.IO.FileMode]::Open, [System.IO.FileAccess]::Read, [System.IO.FileShare]::None)
			if ($fs_ROTest){
				$fs_ROTest.Close()
				if( !([String]::IsNullOrEmpty($LogFile)) ){
					$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] File '$Path' is accessible, READ-only"
				}
      			return 2
			}
		}
		
	}
	catch{
		# fichier verouillé par autre chose
  		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] File '$Path' is locked"
		}
      	return 0
	}
}

function Add-ToCSV{
	<#
	.SYNOPSIS
		ajout de contenu dans un csv
	.DESCRIPTION
		ajout de contenu dans un csv
	.NOTES
		Author: Benoit Sarda
		Date: october 2013
	.PARAMETER Path
		nom du fichier (chemin complet)
	.PARAMETER Content
		contenu à ajouter (array)
	.PARAMETER Delimiter
		caratère de séparation (par défaut ,)
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[String] Path
		[Array] Content
		[String] Delimiter
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[Int] ErrorCode
		ERRORCODES:
			>0 : taille du fichier
			-1: erreur des arguments, mauvais format
			-2: csv locké
			-3: erreur inconnue
	.EXAMPLE
		Add-ToCSV -Path "c:\temp.csv" -Content $csvExport
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Path",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(Mandatory=$true,Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$Path,
		[Parameter(Mandatory=$true,Position=1,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[Array]$Content,
		[Parameter(Mandatory=$false)]
		[String]$Delimiter,
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)

	if( ($Path -isnot [String]) -or ([String]::IsNullOrEmpty($Path)) -or ($Content.Count -lt 1) ){
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect parameters"
		}
		return -1
	}

	if(!($Delimiter)){
		$Delimiter = ","
	}
	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [PROGRESS] The function '$($MyInvocation.InvocationName)' is running"
	}

	# test d'existence du fichier
	if((Test-Path -Path $Path) -eq $false){
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [WARNING] The file '$Path' does not exists, creating"
		}
    	# simple export
		$Content | Export-Csv -Path $Path -NoTypeInformation -NoClobber -Delimiter $Delimiter -Encoding "UTF8"

  	}
	else{
		$isLocked = Test-FileLock -Path $Path
		if( $isLocked -eq 0 ){
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Cannot append to file, locked by another process"
			}
			return -2
		}
		try{
			$Content | ConvertTo-Csv -NoTypeInformation -Delimiter $Delimiter | Select -Skip 1 | Out-File -Append -FilePath $Path -NoClobber -Encoding "UTF8"
		}
		catch{
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Cannot append to file, unknow error"
			}
			return -3
		}
	}
	$fileSize = (New-Object System.IO.FileInfo($Path)).Length
	return $fileSize
}

Export-ModuleMember -Function * -Alias *