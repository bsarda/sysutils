# ----------------------------------------------------------------------------------------------------
function Get-VMsWithVirtualCDRom{
<#
	.SYNOPSIS
		donne le niveau de stats collect du vCenter Server
	.DESCRIPTION
		donne le niveau de stats collect du vCenter Server
	.NOTES
		Author: Benoit Sarda
		Date: april 2017
	.PARAMETER Server
		connected vCenter Server
	.INPUTS
		[VMware.VimAutomation.ViCore.Util10.VersionedObjectImpl] Server
	.OUTPUTS
		[String[]] vmnames

		[Int] ErrorCode
		ERRORCODES:
			-1: bad arguments
	.EXAMPLE
		Get-VMsWithVirtualCDRom -Server $viServer
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Server",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(Position=0,Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[VMware.VimAutomation.ViCore.Util10.VersionedObjectImpl]$Server
	)
	
	$vmnames = Get-View -viewtype virtualmachine|?{($_.Config.Hardware.Device|?{($_.gettype()).name -match "VirtualCdrom"}|%{$_|?{!$_.Connectable.Connected -and $_.DeviceInfo.Summary -notmatch "Remote"}})}|select name
	return $vmnames
}

# ----------------------------------------------------------------------------------------------------
function Disconnect-VMsWithVirtualCDRom{
<#
	.SYNOPSIS
		donne le niveau de stats collect du vCenter Server
	.DESCRIPTION
		donne le niveau de stats collect du vCenter Server
	.NOTES
		Author: Benoit Sarda
		Date: april 2017
	.PARAMETER Server
		connected vCenter Server
	.INPUTS
		[VMware.VimAutomation.ViCore.Util10.VersionedObjectImpl] Server
	.OUTPUTS
		nothing.

		[Int] ErrorCode
		ERRORCODES:
			-1: bad arguments
	.EXAMPLE
		Disconnect-VMsWithVirtualCDRom -Server $viServer
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Server",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(Position=0,Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[VMware.VimAutomation.ViCore.Util10.VersionedObjectImpl]$Server,
		[Parameter(Mandatory=$false)]
		[String[]]$vmnames
	)
	
	if(!$vmnames){
		$vmnames = Get-View -viewtype virtualmachine|?{($_.Config.Hardware.Device|?{($_.gettype()).name -match "VirtualCdrom"}|%{$_|?{!$_.Connectable.Connected -and $_.DeviceInfo.Summary -notmatch "Remote"}})}|select name
	}
	$vmnames | %{Get-VM $_.name|Get-CDDrive|Set-CDDrive -NoMedia -Confirm:$false}
}
