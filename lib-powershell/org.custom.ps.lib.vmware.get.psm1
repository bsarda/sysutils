# ----------------------------------------------------------------------------------------------------
function Get-StatLevels{
<#
	.SYNOPSIS
		donne le niveau de stats collect du vCenter Server
	.DESCRIPTION
		donne le niveau de stats collect du vCenter Server
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Server
		vcenter server connecté
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[VMware.VimAutomation.ViCore.Util10.VersionedObjectImpl] Server
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[Hashtable] keys=day,week,month,year value=values

		[Int] ErrorCode
		ERRORCODES:
			-1: erreur des arguments, mauvais format
			-2: erreur de récupération des informations de statistiques
	.EXAMPLE
		Get-StatLevels -Server $viServer
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Server",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(Position=0,Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[VMware.VimAutomation.ViCore.Util10.VersionedObjectImpl]$Server,
		[Parameter(Mandatory=$false)]
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)

	if( $Server -isnot [VMware.VimAutomation.ViCore.Util10.VersionedObjectImpl] ){
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect parameters"
		}
		return -1
	}
	
	try{
		# vue de l'instance sur le serveur fourni
		$o_viewServiceInstance = Get-View -Id ServiceInstance -Server $Server
		# retrouver la vue du perfmanager
		$o_perfCounter = Get-View ($o_viewServiceInstance).Content.PerfManager
		# récupérer les informations d'historiques
		$ao_historicalInterval = $o_perfCounter.HistoricalInterval
	}
	catch{
		return -2
	}
	# construction du hashtable de retour
	$h_historicalInterval = @{}
	$h_historicalInterval["day"] = ($ao_historicalInterval | ?{$_.Key -eq 1}).Level
	$h_historicalInterval["week"] = ($ao_historicalInterval | ?{$_.Key -eq 2}).Level
	$h_historicalInterval["month"] = ($ao_historicalInterval | ?{$_.Key -eq 3}).Level
	$h_historicalInterval["year"] = ($ao_historicalInterval | ?{$_.Key -eq 4}).Level
	# retour du hash
	return $h_historicalInterval
}

# ----------------------------------------------------------------------------------------------------
function Get-HashtablesFromInventory{
	<#
	.SYNOPSIS
		renvoie des hashtables d'association sur l'inventaire
	.DESCRIPTION
		renvoie des hashtables d'association sur l'inventaire
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Inventory
		liste de l'inventaire telle que récupérée avec un Get-Inventory
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[String] Server
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[Array]
			[0][HashTable] ArrayIndex<->Hashtable-name
			[1][HashTable] Datacenter-ID<->Datacenter-name
			[2][HashTable] Cluster-ID<->Datacenter-ID
			[3][HashTable] Cluster-ID<->Cluster-name
			[4][HashTable] Host-ID<->Cluster-ID
			[5][HashTable] Host-ID<->Hostname
			[6][HashTable] wwpn<->Host-ID
			[7][HashTable] VM-ID<->Cluster-ID
			[8][HashTable] VM-ID<->Host-ID
			[9][HashTable] VM-ID<->VM-name
			[10][HashTable] VM-ID<->Guest-name
			[11][HashTable] HostID/naa<->Multipath-policy
			[12][HashTable] HostID/naa<->RuntimeName
			[13][HashTable] HostID/naa<->Vendor
			[14][HashTable] HostID/naa<->Model
			[15][HashTable] HostID/naa<->VAAI-Status
			[16][HashTable] HostID/naa<->VAAI-Plugin
			[17][HashTable] HostID/Host-wwpn/Lun-wwpn/naa<->ConnexionStatus
			[18][HashTable] HostID/Host-wwpn/Lun-wwpn/naa<->IsPreferedPath
		[Int] ErrorCode
		ERRORCODES:
			-1: erreur des arguments, mauvais format
	.EXAMPLE
		Get-HashtablesFromInventory $InventoryFull
	.LINK
		b.sarda@free.fr
	#>
	[CmdletBinding(
		DefaultParameterSetName="Inventory",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(Position=0,Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[Array]$Inventory,
		[Parameter(Mandatory=$false)]
		[Bool]$LunPathInfos = $false,
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)
	
	if( $Inventory -isnot [Array] ){
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect parameters"
		}
		return -1
	}
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Function is starting..."
	}
	# init des hashtables
	$h_datacenterid_datacentername = @{}	# Datacenter-datacenter-12 <=> DC NY
	$h_clusterid_datacenterid = @{}			# 
	$h_clusterid_clustername = @{}			# 
	$h_hostid_clusterid = @{}				# HostSystem-host-124 <=> Folder-host-14
	$h_hostid_hostname = @{}				# HostSystem-host-124 <=> adx-labb-srv01.deskserv.atelier
	$h_wwpn_hostid = @{}					# 210000E08B93225D <=> HostSystem-host-124
	$h_vmid_clusterid = @{}					# VirtualMachine-vm-43 <=> MgmtPod
	$h_vmid_hostid = @{}
	$h_vmid_vmname = @{}					# VirtualMachine-vm-43 <=> Test2_VM
	$h_vmid_guestname = @{}
	$h_hostidnaa_multipath = @{}
	$h_hostidnaa_runtime = @{}
	$h_hostidnaa_vendor = @{}
	$h_hostidnaa_model = @{}
	$h_hostidnaa_vaaistatus = @{}			# avec les chiffres de VAAIstatus => 1.ATS, 2.Clone, 3.Delete, 4.Zero
	$h_hostidnaa_vaaiplugin = @{}
	$h_hostidpath_status = @{}				# avec hostidpath = hostId / wwpn du host / sanId
	$h_hostidpath_prefered = @{}

	# pour chaque Datacenter
	foreach($o_datacenter in ($Inventory | where{ ($_.GetType()).Name -eq "DatacenterWrapper"}) ){
		# correspondance datacenter Id <-> Name
		$h_datacenterid_datacentername[$o_datacenter.Id] = $o_datacenter.Name
	}
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Datacenter collection finished"
	}
	# pour chaque Cluster
	foreach($o_cluster in ($Inventory | where{ ($_.GetType()).Name -eq "ClusterWrapper"}) ){
		# correspondance Cluster Id <-> Datacenter Id ==> donne le datacenter parent du cluster
		$h_clusterid_datacenterid[$o_cluster.Id] = $o_datacenter.Id
		# correspondance Cluster Id <-> Name
		$h_clusterid_clustername[$o_cluster.Id] = $o_cluster.Name
	}
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Cluster collection finished"
	}
	# pour chaque Host
	foreach($o_host in ($Inventory | where{ ($_.GetType()).Name -eq "VMHostWrapper"}) ){
		# correspondance host Id <-> Cluster Id ==> donne le cluster parent du host
		$h_hostid_clusterid[$o_host.Id] = $o_host.Parent.Id
		# correspondance host Id <-> Name
		$h_hostid_hostname[$o_host.Id] = $o_host.Name
		# pour chaque hba (donc de chaque host)
		foreach( $o_hba in $o_host.ExtensionData.Config.StorageDevice.HostBusAdapter){
			# si le hba est de type fc
			if( $o_hba.GetType().Name -eq "HostFibreChannelHba" ){
				# on convertit en hexa et on ajoute au hash avec le WWpn en key et le nom du host en value
				$x_wwpn = ($o_hba.PortWorldWideName).ToString("X")
				# correspondance WWPN <-> host Id ==> donne le host parent du wwpn
				$h_wwpn_hostid[$x_wwpn] = $o_host.Id
			}
		}
		if( $LunPathInfos ){
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Mode LunPathInfos enabled, exection will be longer"
			}
			# pour chaque lun scsi
			# retrouver les infos de toutes les luns liées au host
			$o_scsiLuns = Get-ScsiLun -VmHost $o_host -LunType disk
			# ouvrir un esxcli
			$o_esxcli = Get-EsxCli -VMHost $o_host
			foreach( $o_scsiLun in $o_scsiLuns ){
				# pour chaque Lun SCSI
				$h_hostidnaa_multipath[$o_scsiLun.Id] = $o_scsiLun.MultipathPolicy
				$h_hostidnaa_runtime[$o_scsiLun.Id] = $o_scsiLun.RuntimeName
				$h_hostidnaa_vendor[$o_scsiLun.Id] = $o_scsiLun.Vendor
				$h_hostidnaa_model[$o_scsiLun.Id] = $o_scsiLun.Model
				# on se sert de l'esxcli pour retrouver le statut VAAI (ATSStatus, CloneStatus, DeleteStatus, ZeroStatus)
				$ao_vaaiStatus = ($o_esxcli.storage.core.device.vaai.status.get($o_scsiLun.CanonicalName))
				$s_vaaiStatus = ""
				if($ao_vaaiStatus.ATSStatus -eq "supported"){
					$s_vaaiStatus += "1"
				}
				else{
					$s_vaaiStatus += "0"
				}
				if($ao_vaaiStatus.CloneStatus -eq "supported"){
					$s_vaaiStatus += "1"
				}
				else{
					$s_vaaiStatus += "0"
				}
				if($ao_vaaiStatus.DeleteStatus -eq "supported"){
					$s_vaaiStatus += "1"
				}
				else{
					$s_vaaiStatus += "0"
				}
				if($ao_vaaiStatus.ZeroStatus -eq "supported"){
					$s_vaaiStatus += "1"
				}
				else{
					$s_vaaiStatus += "0"
				}
				$h_hostidnaa_vaaiplugin[$o_scsiLun.Id] = $ao_vaaiStatus.VAAIPluginName
				$h_hostidnaa_vaaistatus[$o_scsiLun.Id] = $s_vaaiStatus
				# retrouver les informations de chemins, pour chaque LUN
				$ao_ScsiLunPaths = Get-ScsiLunPath -ScsiLun $o_scsiLun
				foreach( $o_ScsiLunPath in $ao_ScsiLunPaths){
					if( ($o_ScsiLunPath -cnotlike "pscsi.*") -or ($o_ScsiLunPath -cnotlike "sas.*") ){
						try{
							$s_HostIdPath = $o_scsiLun.VMHostId + "/" + ((($o_ScsiLunPath.Name).Split("-"))[0].Split(":"))[1].ToUpper() + "/" + ($o_ScsiLunPath.SanId).Replace(":","") + "/" + $o_scsiLun.CanonicalName
							$h_hostidpath_status[$s_HostIdPath] = $o_ScsiLunPath.State
							$h_hostidpath_prefered[$s_HostIdPath] = $o_ScsiLunPath.Preferred
						}
						catch{
							if( !([String]::IsNullOrEmpty($LogFile)) ){
								$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [WARNING] Cannot retreive status of Lun Path: $($o_ScsiLunPath.Name), HostId: $($o_scsiLun.VMHostId), SanId: $($o_ScsiLunPath.SanId)"
							}
						}
					}
				}
			}
		}
	}
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Host collection finished"
	}
	# pour chaque VM
	foreach($o_vm in ($Inventory | where{ ($_.GetType()).Name -eq "VirtualMachineWrapper"}) ){
		# on ajoute l'id de la vm en key et son nom en value
		# correspondance VM Id <-> Name
		$h_vmid_vmname[$o_vm.Id] = $o_vm.Name
		# correspondance VM_Id <-> Host_ID
		$h_vmid_hostid[$o_vm.Id] = $o_vm.VMHost.Id
		# correspondance VM Id <-> hostname ==> donne le hostname(guestOS) de la VM
		if(!([String]::IsNullOrEmpty($o_vm.Guest.HostName) )){
			$h_vmid_guestname[$o_vm.Id] = $o_vm.Guest.HostName
		}
		else{
			$h_vmid_guestname[$o_vm.Id] = $o_vm.Name
		}
		# correspondance VM Id <-> Cluster_Id ==> donne le host parent de la VM
		$h_vmid_clusterid[$o_vm.Id] = $o_vm.VMHost.Parent.Id
	}
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] VMs collection finished"
	}
	# tableau d'indexs
	$h_returnIndexes = @{}
	$h_returnIndexes["Datacenter-ID<->Datacenter-name"] = 1
	$h_returnIndexes["Cluster-ID<->Datacenter-ID"] = 2
	$h_returnIndexes["Cluster-ID<->Cluster-name"] = 3
	$h_returnIndexes["Host-ID<->Cluster-ID"] = 4
	$h_returnIndexes["Host-ID<->Hostname"] = 5
	$h_returnIndexes["wwpn<->Host-ID"] = 6
	$h_returnIndexes["VM-ID<->Cluster-ID"] = 7
	$h_returnIndexes["VM-ID<->Host-ID"] = 8
	$h_returnIndexes["VM-ID<->VM-name"] = 9
	$h_returnIndexes["VM-ID<->Guest-name"] = 10
	$h_returnIndexes["HostID/naa<->Multipath-policy"] = 11
	$h_returnIndexes["HostID/naa<->RuntimeName"] = 12
	$h_returnIndexes["HostID/naa<->Vendor"] =13
	$h_returnIndexes["HostID/naa<->Model"] = 14
	$h_returnIndexes["HostID/naa<->VAAI-Status"] = 15
	$h_returnIndexes["HostID/naa<->VAAI-Plugin"] = 16
	$h_returnIndexes["HostID/Host-wwpn/Lun-wwpn/naa<->ConnexionStatus"] = 17
	$h_returnIndexes["HostID/Host-wwpn/Lun-wwpn/naa<->IsPreferedPath"] = 18
	# retour des tableaux (meme si vides)
	$ah_returnHash = @( $h_returnIndexes, `
						$h_datacenterid_datacentername,$h_clusterid_datacenterid,$h_clusterid_clustername,$h_hostid_clusterid,$h_hostid_hostname,$h_wwpn_hostid, `
						$h_vmid_clusterid,$h_vmid_hostid,$h_vmid_vmname,$h_vmid_guestname, `
						$h_hostidnaa_multipath,$h_hostidnaa_runtime,$h_hostidnaa_vendor,$h_hostidnaa_model,$h_hostidnaa_vaaistatus,$h_hostidnaa_vaaiplugin, `
						$h_hostidpath_status,$h_hostidpath_prefered )
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Function is ending..."
	}
	return $ah_returnHash
}

# ----------------------------------------------------------------------------------------------------
function Get-HashtablesFromDatastores{
	<#
	.SYNOPSIS
		renvoie des hashtables d'association sur les datastores
	.DESCRIPTION
		renvoie des hashtables d'association sur les datastores
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Datastores
		liste des datastores telle que récupérée avec un Get-Datastore
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[String] Server
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[Array]
			[0][HashTable] ArrayIndex<->Hashtable-name
			[1][HashTable] Datastore-ID<->Datacenter-ID
			[2][HashTable] Datastore-ID<->Datastore-name
			[3][HashTable] Datastore-UUID<->Datastore-ID
			[4][HashTable] NAA<->Datastore-ID
			
		[Int] ErrorCode
		ERRORCODES:
			-1: erreur des arguments, mauvais format
	.EXAMPLE
		Get-HashtablesFromDatastores $DSList
	.LINK
		b.sarda@free.fr
	#>
	[CmdletBinding(
		DefaultParameterSetName="Datastores",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(Position=0,Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[Array]$Datastores,
		[Parameter(Mandatory=$false)]
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)
	
	if( $Datastores -isnot [Array] ){
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect parameters"
		}
		return -1
	}
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Function is starting..."
	}
	# init des hashtables
	$h_datastoreid_datacenterid = @{}		# Datastore-datastore-142 <=> Datacenter-datacenter-12
	$h_datastoreid_datastorename = @{}		# Datastore-datastore-142 <=> Prod-DS4700-DPE01_LUN01
	$h_datastoreuuid_datastoreid = @{}		# 520e1ad3-0b29ba40-bb72-00215a9b0144 <=> Datastore-datastore-142
	$h_naa_datastoreid = @{}				# naa.600a0b8000262b0400000dea500d0d84 <=> Datastore-datastore-142
	$h_datastorekey_datastore

	# pour chaque Datastore
	foreach($o_datastore in $ao_inventoryDatastores ){
		# correspondance datastore Id <-> Name
		$h_datastoreid_datastorename[$o_datastore.Id] = $o_datastore.Name
		# correspondance signature <-> id
		$h_datastoreuuid_datastoreid[($o_datastore.ExtensionData.Info.Vmfs.Uuid).ToString()] = $o_datastore.Id
		# correspondance datastore Id <-> datacenter Id ==> donne le datacenter parent du datastore
		$h_datastoreid_datacenterid[$o_datastore.Id] = $o_datastore.DatacenterId
		# pour chaque extent du datastore
		$o_datastore.ExtensionData.Info.Vmfs.Extent | %{
			# correspondance datastore NAA <-> Id ==> donne le datastore parent de la LUN identifiée par son naa/t10
			$h_naa_datastoreid[$_.DiskName] = $o_datastore.Id
		}
	}
	# tableau d'indexs	
	$h_returnIndexes = @{}
	$h_returnIndexes["Datastore-ID<->Datacenter-ID"] = 1
	$h_returnIndexes["Datastore-ID<->Datastore-name"] = 2
	$h_returnIndexes["Datastore-UUID<->Datastore-ID"] = 3
	$h_returnIndexes["NAA<->Datastore-ID"] = 4
	# retour des tableaux
	$ah_returnHash = @( $h_returnIndexes, `
						$h_datastoreid_datacenterid,$h_datastoreid_datastorename,$h_datastoreuuid_datastoreid,$h_naa_datastoreid)
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Function is ending..."
	}
	return $ah_returnHash
}

# ----------------------------------------------------------------------------------------------------
function Get-HashtablesFromHarddisks{
	<#
	.SYNOPSIS
		renvoie des hashtables d'association sur les disques virtuels des vms
	.DESCRIPTION
		renvoie des hashtables d'association sur les disques virtuels des vms
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Harddisks
		liste des datastores telle que récupérée avec un Get-HardDisk
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[String] Server
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[Array]
			[0][HashTable] ArrayIndex<->Hashtable-name
			[1][HashTable] vHardDisk-NAA(RDM)<->VM-ID/Controller-Disk-ID
			[2][HashTable] vHardDisk-NAA(RDM)<->Scsi-ID
			[3][HashTable] VM-ID/Scsi-ID<->VM-ID/Controller-Disk-ID
			[4][HashTable] VM-ID/Controller-Disk-ID<->VM-ID
			[5][HashTable] VM-ID/Controller-Disk-ID<->VM-ID/Scsi-ID
			[6][HashTable] VM-ID/Controller-Disk-ID<->Datastore-ID
			[7][HashTable] VM-ID/Controller-Disk-ID<->HardDisk-Number
			[7][HashTable] VM-ID/Key-ID<->VM-ID/Controller-Disk-ID
			
		[Int] ErrorCode
		ERRORCODES:
			-1: erreur des arguments, mauvais format
	.EXAMPLE
		Get-HashtablesFromHarddisks $HDList
	.LINK
		b.sarda@free.fr
	#>
	[CmdletBinding(
		DefaultParameterSetName="HardDisks",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(Position=0,Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[Array]$HardDisks,
		[Parameter(Mandatory=$false)]
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)
	
	if( $HardDisks -isnot [Array] ){
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect parameters"
		}
		return -1
	}
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Function is starting..."
	}
	# init des hashtables
	$h_naa_vdisk = @{}						# t10.FreeBSD_iSCSI_Disk______000c296bb721000_________________ <=> VirtualMachine-vm-43/2008
	$h_naa_vscsi = @{}						# t10.FreeBSD_iSCSI_Disk______000c296bb721000_________________ <=> scsi1:0
	$h_vmidvscsi_vdisk = @{}				# VirtualMachine-vm-43/scsi0:1 <=> VirtualMachine-vm-43/2016
	$h_vdisk_vmid = @{}
	$h_vdisk_vscsi = @{}					# VirtualMachine-vm-162/2016 <=> scsi0:1
	$h_vdisk_datastore = @{}				# VirtualMachine-vm-34/2003 <-> datastore-104
	$h_vdisk_hdnum = @{}
	$h_keyid_vdisk = @{}
	
	# constantes
	$IDE_CTRL_KEY = 200
	$SCSI_CTRL_KEY = 1000

	# pour chaque vDisk
	foreach($o_vdisk in $HardDisks ){
		# correspondance vdisk Id <-> VM Id ==> donne la VM parente du vDisk
		$h_vdisk_vmid[$o_vdisk.Id] = $o_vdisk.Parent.Id
		# correspondance vdisk Id <-> datastore Id ==> donne le datastore qui contient le vdisk
		$h_vdisk_datastore[$o_vdisk.Id] = $o_vdisk.ExtensionData.Backing.Datastore.Value
		# si on a un controlleur/disque IDE ou SCSI, les ID et Keys seront pas gérées pareil
		# cas IDE, controlleur à partir de ID 200, max 1 controlleur par VM
		if( ($o_vdisk.ExtensionData.ControllerKey -ge $IDE_CTRL_KEY) -and ($o_vdisk.ExtensionData.ControllerKey -le ($IDE_CTRL_KEY+1)) ){
			# correspondance vdisk Id <-> scsiId ==> donne le node scsi associé au vdisk
			$h_vdisk_vscsi[$o_vdisk.Id] = $o_vdisk.Parent.Id + "/ide" + ($o_vdisk.ExtensionData.ControllerKey - $IDE_CTRL_KEY) + ":" + ($o_vdisk.ExtensionData.UnitNumber)
			# correspondance (VM Id / scsiId) <-> vDisk Id ==> donne l'Id du vDisk associé au node scsi d'une VM (cumul)
			$h_vmidvscsi_vdisk[($o_vdisk.Parent.Id + "/ide" + ($o_vdisk.ExtensionData.ControllerKey - $IDE_CTRL_KEY) + ":" + ($o_vdisk.ExtensionData.UnitNumber))] = $o_vdisk.Id
			# si c'est un RDM
			if( $o_vdisk.ScsiCanonicalName ){
				# correspondance NAA <-> vDisk Id ==> donne le vdisk associé au RDM
				$h_naa_vdisk[$o_vdisk.ScsiCanonicalName] = $o_vdisk.Id
				# correspondance vdisk Id <-> (VM Id / scsiId) ==> donne la vm / node scsi associé au RDM
				$h_naa_vscsi[$o_vdisk.ScsiCanonicalName] = $o_vdisk.Parent.Id + "/ide" + ($o_vdisk.ExtensionData.ControllerKey - $IDE_CTRL_KEY) + ":" + ($o_vdisk.ExtensionData.UnitNumber)
			}
		}
		# cas SCSI, controlleur à partir de ID 1000, max 4 controlleurs par VM
		else{
			# correspondance vdisk Id <-> scsiId ==> donne le node scsi associé au vdisk
			$h_vdisk_vscsi[$o_vdisk.Id] = $o_vdisk.Parent.Id + "/scsi" + ($o_vdisk.ExtensionData.ControllerKey - $SCSI_CTRL_KEY) + ":" + ($o_vdisk.ExtensionData.UnitNumber)
			# correspondance (VM Id / scsiId) <-> vDisk Id ==> donne l'Id du vDisk associé au node scsi d'une VM (cumul)
			$h_vmidvscsi_vdisk[($o_vdisk.Parent.Id + "/scsi" + ($o_vdisk.ExtensionData.ControllerKey - $SCSI_CTRL_KEY) + ":" + ($o_vdisk.ExtensionData.UnitNumber))] = $o_vdisk.Id
			# si c'est un RDM
			if( $o_vdisk.ScsiCanonicalName ){
				# correspondance NAA <-> vDisk Id ==> donne le vdisk associé au RDM
				$h_naa_vdisk[$o_vdisk.ScsiCanonicalName] = $o_vdisk.Id
				# correspondance vdisk Id <-> (VM Id / scsiId) ==> donne la vm / node scsi associé au RDM
				$h_naa_vscsi[$o_vdisk.ScsiCanonicalName] = $o_vdisk.Parent.Id + "/scsi" + ($o_vdisk.ExtensionData.ControllerKey - $SCSI_CTRL_KEY) + ":" + ($o_vdisk.ExtensionData.UnitNumber)
			}
		}
		# retrouver le "Hard disk 1"
		if( $o_vdisk.Name -like "Disque dur*" ){
			$h_vdisk_hdnum[$o_vdisk.Id] = ($o_vdisk.Name).Replace("Disque dur","Hard disk")
		}
		else{
			$h_vdisk_hdnum[$o_vdisk.Id] = $o_vdisk.Name
		}
		# map key-controler
		$h_keyid_vdisk[$o_vdisk.ParentId +"/"+ $o_vdisk.ExtensionData.Key] = $o_vdisk.Id
	}
	# tableau d'indexs
	$h_returnIndexes = @{}
	$h_returnIndexes["vHardDisk-NAA(RDM)<->VM-ID/Controller-Disk-ID"] = 1
	$h_returnIndexes["vHardDisk-NAA(RDM)<->Scsi-ID"] = 2
	$h_returnIndexes["VM-ID/Scsi-ID<->VM-ID/Controller-Disk-ID"] = 3
	$h_returnIndexes["VM-ID/Controller-Disk-ID<->VM-ID"] = 4
	$h_returnIndexes["VM-ID/Controller-Disk-ID<->VM-ID/Scsi-ID"] = 5
	$h_returnIndexes["VM-ID/Controller-Disk-ID<->Datastore-ID"] = 6
	$h_returnIndexes["VM-ID/Controller-Disk-ID<->HardDisk-Number"] = 7
	$h_returnIndexes["VM-ID/Key-ID<->VM-ID/Controller-Disk-ID"] = 8
	# retour des tableaux (meme si vides)
	$ah_returnHash = @( $h_returnIndexes, `
						$h_naa_vdisk,$h_naa_vscsi,$h_vmidvscsi_vdisk,$h_vdisk_vmid,$h_vdisk_vscsi,$h_vdisk_datastore,$h_vdisk_hdnum,$h_keyid_vdisk)
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Function is ending..."
	}
	return $ah_returnHash
}

# ----------------------------------------------------------------------------------------------------
function Get-VMInfos{
	<#
	.SYNOPSIS
		donne les infos intéressantes sur une VM particulière
	.DESCRIPTION
		donne les infos intéressantes sur une VM particulière
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER VM
		objet machine virtuelle en entrée (faire un get-vm avant si besoin)
	.PARAMETER Name
		nom de la VM (à la place de l'objet)
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[VMware.VimAutomation.ViCore.Impl.V1.Inventory.InventoryItemImpl] VM
		[String] Name
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[Array]
			[0][Array] configuration de la VM
			[1][Array] configuration des vDisks
		[Int] ErrorCode
		ERRORCODES:
			0 : exécution OK
			-1: erreur des arguments, mauvais format
			-2: erreur inconnue durant l'init de la déconnexion
	.EXAMPLE
		Get-VMInfos
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="VM",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(ParameterSetName="VMObj",Mandatory=$true,Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[VMware.VimAutomation.ViCore.Impl.V1.Inventory.InventoryItemImpl]$VM,
		[Parameter(ParameterSetName="VMName",Mandatory=$true,Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$Name,
		[Parameter(Mandatory=$false)]
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)

	# si les paramètres ne sont pas au bon format
	if( ($Name -isnot [String]) -and ($VM -isnot [VMware.VimAutomation.ViCore.Util10.VersionedObjectImpl]) ){
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect parameter"
		}
		return -1
	}
<#
	# définition des variables: pour chaque vHBA en NPIV sur la VM
	$confVHBAObjectsList = ("wwpn","wwnn","Owner")
	# définition des variables: pour chaque VMNIC
	$confVNetObjectsList = ("Type","MAC","Owner","GuestIP","PortId","PortGroup","vSwitch")
	# création de la hashtable avec les keys pré-créées
	$h_confVHBA = @{}
	$confVHBAObjectsList | %{ $h_confVHBA.Add($_,"") }
	# création de la hashtable avec les keys pré-créées
	$h_confVNet = @{}
	$confVNetObjectsList | %{ $h_confVNet.Add($_,"") }
#>

	# récupération du nom ou de la VM
	if($VM){
		$Name = $VM.Name
	}
	elseif($Name){
		try{
			$VM = Get-VM -Name $Name
		}
		catch{
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Cannot retrieve VM from name"
			}
			return -2
		}
	}
	else{
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect parameter"
		}
		return -1
	}
	# définition du string-tableau
	$ao_confVMX = "" | Select-Object -Property vCPUTotal,vSockets,vCoresPerSocket,vCPUReserved,vCPUExpandable,vCPULimit,vCPUSharesNum,vCPUSharesLevel,vCPUAffinity,vCPUHotAdd, `
							vRAMMB,vRAMReserved,vRAMExpandable,vRAMLimit,vRAMSharesNum,vRAMSharesLevel,vRAMAffinity,vRAMHotAdd,vHWversion, `
							VMHost,VMHostId,ResourcePool,ResourcePoolId,Folder,FolderId,Name,MoRef,PowerState, `
							UsageCPUDemand,UsageCPU,UsageCPUMax,UsageMemMB,UsageMemGuestMB,UsageMemMaxMB,UsageMemSharedMB,UsageMemSwappedMB,UsageMemBalloonMB,UsageOverheadMemory, `
							GuestBootTime,GuestHeartbeat,GuestUptime,GuestOSConfigured,GuestOSReal,GuestIPs,GuestHostname,GuestToolsVer,GuestToolsRun, `
							StorageVMXPath,StorageSwapPath,StorageSnapshotPath,StorageTotalProvisionned,StorageTotalUsage,StorageInGuestUsage,StorageInGuestFree
	
	
	
	# maintenant qu'on a le getview on ajoute toutes les properties
		# ajout des propriétés vCPU
		$ao_confVMX.vCPUTotal = $VM.NumCpu
		$ao_confVMX.vSockets = $VM.ExtensionData.Config.Hardware.NumCPU
		$ao_confVMX.vCoresPerSocket = $VM.ExtensionData.Config.Hardware.NumCoresPerSocket
		$ao_confVMX.vCPUReserved = $VM.VMResourceConfiguration.CpuReservationMhz
		$ao_confVMX.vCPUExpandable = $VM.ExtensionData.Config.CpuAllocation.ExpandableReservation
		$ao_confVMX.vCPULimit = $VM.VMResourceConfiguration.CpuLimitMhz
		$ao_confVMX.vCPUSharesNum = $VM.VMResourceConfiguration.NumCpuShares
		$ao_confVMX.vCPUSharesLevel = ($VM.VMResourceConfiguration.CpuSharesLevel).ToString()
		$ao_confVMX.vCPUAffinity = $VM.VMResourceConfiguration.CpuAffinityList
		$ao_confVMX.vCPUHotAdd = $VM.ExtensionData.Config.CpuHotAddEnabled
		# ajout des propriétés vRAM
		$ao_confVMX.vRAMMB = $VM.MemoryMB
		$ao_confVMX.vRAMReserved = $VM.VMResourceConfiguration.MemReservationMB
		$ao_confVMX.vRAMExpandable = $VM.ExtensionData.Config.MemoryAllocation.ExpandableReservation
		$ao_confVMX.vRAMLimit = $VM.VMResourceConfiguration.MemLimitMB
		$ao_confVMX.vRAMSharesNum = $VM.VMResourceConfiguration.NumMemShares
		$ao_confVMX.vRAMSharesLevel = ($VM.VMResourceConfiguration.MemSharesLevel).ToString()
		$ao_confVMX.vRAMAffinity = $VM.ExtensionData.Config.MemoryAffinity
		$ao_confVMX.vRAMHotAdd = $VM.ExtensionData.Config.MemoryHotAddEnabled
		$ao_confVMX.vHWversion = ($VM.ExtensionData.Config.Version).ToString()
		# ajout des propriétés Inventory
		$ao_confVMX.VMHost = ($VM.VMHost).ToString()
		$ao_confVMX.VMHostId = $VM.VMHostId
		if(!([String]::IsNullOrEmpty($VM.ResourcePool)) ){
			$ao_confVMX.ResourcePool = ($VM.ResourcePool).ToString()
		}
		else{
			$ao_confVMX.ResourcePool = $null
		}
		$ao_confVMX.ResourcePoolId = $VM.ResourcePoolId
		
		try{
			$ao_confVMX.Folder = ($VM.Folder).ToString()
		}
		catch{
			$ao_confVMX.Folder = $null
		}
				
		$ao_confVMX.FolderId = $VM.FolderId
		$ao_confVMX.Name = $VM.Name
		$ao_confVMX.MoRef = $VM.Id
		$ao_confVMX.PowerState = ($VM.PowerState).ToString()
		# ajout des quickstats usage
		$ao_confVMX.UsageCPU = $VM.ExtensionData.Summary.QuickStats.OverallCpuUsage
		$ao_confVMX.UsageCPUDemand = $VM.ExtensionData.Summary.QuickStats.OverallCpuDemand
		$ao_confVMX.UsageCPUMax = $VM.ExtensionData.Summary.Runtime.MaxCpuUsage
		$ao_confVMX.UsageMemGuestMB = $VM.ExtensionData.Summary.QuickStats.GuestMemoryUsage
		$ao_confVMX.UsageMemMB = $VM.ExtensionData.Summary.QuickStats.HostMemoryUsage
		$ao_confVMX.UsageMemMaxMB = $VM.ExtensionData.Summary.Runtime.MaxMemoryUsage
		$ao_confVMX.UsageMemSharedMB = $VM.ExtensionData.Summary.QuickStats.SharedMemory
		$ao_confVMX.UsageMemSwappedMB = $VM.ExtensionData.Summary.QuickStats.SwappedMemory
		$ao_confVMX.UsageMemBalloonMB = $VM.ExtensionData.Summary.QuickStats.BalloonedMemory
		$ao_confVMX.UsageOverheadMemory = $VM.ExtensionData.Summary.Runtime.MemoryOverhead
		# ajout des propriétés guestOS
		$ao_confVMX.GuestBootTime = $VM.ExtensionData.Summary.Runtime.BootTime
		$ao_confVMX.GuestHeartbeat = ($VM.ExtensionData.Summary.QuickStats.GuestHeartbeatStatus).ToString()
		$ao_confVMX.GuestUptime = $VM.ExtensionData.Summary.QuickStats.UptimeSeconds
		$ao_confVMX.GuestOSConfigured = $VM.ExtensionData.Config.GuestFullName
		$ao_confVMX.GuestOSReal = $VM.ExtensionData.Summary.Guest.GuestFullName
		# collecte des IPs
		$ao_confVMX.GuestIPs = ""
		foreach( $vmeth in $VM.ExtensionData.Guest.Net){
			if( $vmeth.IpAddress -ne $null){
				$vmeth.IpAddress | %{ $ao_confVMX.GuestIPs += "/" + $_.ToString() }
				$ao_confVMX.GuestIPs = ($ao_confVMX.GuestIPs).TrimStart('|')
			}
			else{
				$ao_confVMX.GuestIPs = $null
			}
		}
		$ao_confVMX.GuestHostname = $VM.ExtensionData.Summary.Guest.HostName
		$ao_confVMX.GuestToolsVer = $VM.ExtensionData.Summary.Guest.ToolsVersionStatus
		$ao_confVMX.GuestToolsRun = $VM.ExtensionData.Summary.Guest.ToolsRunningStatus
		# ajout des propriétés stockage
		$ao_confVMX.StorageVMXPath = $VM.ExtensionData.Config.Files.VmPathName
		$ao_confVMX.StorageSwapPath = $VM.ExtensionData.Layout.SwapFile
		$ao_confVMX.StorageSnapshotPath = $VM.ExtensionData.Config.Files.SnapshotDirectory
		$ao_confVMX.StorageTotalUsage = $VM.ExtensionData.Summary.Storage.Committed
		$ao_confVMX.StorageInGuestUsage = [Int64](($VM.Guest.Disks | Measure-Object -Property Capacity -Sum).Sum)
		$ao_confVMX.StorageInGuestFree = [Int64](($VM.Guest.Disks | Measure-Object -Property FreeSpace -Sum).Sum)
		$ao_confVMX.StorageTotalProvisionned = $VM.ExtensionData.Summary.Storage.Uncommitted + $VM.ExtensionData.Summary.Storage.Committed

	# la conf npiv
	$ao_confNPIV = @()
	# si la coche NPIV Temporary Disabled est false
	if( $VM.ExtensionData.Config.NpivTemporaryDisabled -eq $false){
		# pour charque wwnn
		$o_confNPIV = "" | Select-Object -Property WWPN, WWNN
		$o_confNPIV.WWNN = $VM.ExtensionData.Config.NpivNodeWorldWideName
		$o_confNPIV.WWPN = $VM.ExtensionData.Config.NpivPortWorldWideName
		$ao_confNPIV = $o_confNPIV
	}

	# tableau des disques
	$ao_confVDisks = @()
	# pour chaque disque de la VM
	foreach($VMHardDisk in Get-HardDisk -VM $VM){
		# création du string-tableau
		$as_confVDisk = "" | Select-Object -Property SharesNum,SharesLevel,IOPSLimit, `
													Key,Owner,Persistence,CapacityAllocated,CapacityConsummed, `
													VMDKName,VMDKFilePath,VMDKFullPath,Datastore,DatastoreId,StorageFormat,StorageNAA
		
		# on retrouve le disque courant des conf ressources (l'ordre n'est pas le même)
		$VMHardDiskResource = ( $VM.VMResourceConfiguration.DiskResourceConfiguration | ?{ $_.Key -eq (($VMHardDisk.Id.Split("/"))[1]) } )
		# split pour trouver owner/key
		$splitKeyOwner = $VMHardDisk.Id.Split("/")
		# on remplit les champs
		$as_confVDisk.Key = $splitKeyOwner[1]
		$as_confVDisk.SharesNum = $VMHardDiskResource.NumDiskShares
		$as_confVDisk.SharesLevel = ($VMHardDiskResource.DiskSharesLevel).ToString()
		$as_confVDisk.IOPSLimit = $VMHardDiskResource.DiskLimitIOPerSecond
		$as_confVDisk.Owner = $splitKeyOwner[0]
		$as_confVDisk.VMDKFullPath = $VMHardDisk.Filename
		$as_confVDisk.VMDKFilePath = ($VMHardDisk.Filename.Split(' '))[1].Trim()
		$as_confVDisk.VMDKName = $VMHardDisk.Name
		$as_confVDisk.Persistence = ($VMHardDisk.Persistence).ToString()
		

		if( $VMHardDisk.ScsiCanonicalName ){
			# si c'est un RDM, allocated=consummed car pas de vision de ce qui est réellement consommé
			$as_confVDisk.CapacityAllocated = $i_vdiskConsumedSize
			$as_confVDisk.CapacityConsummed = $VMHardDisk.CapacityKB*1024
		}
		elseif( $VM.ExtensionData.Snapshot -ne $null ){
			# s'il y a un snapshot
			# on fait une boucle: tant q'il y a des parents, c'est de l'espace utile
			$as_disksFiles = @()
			$as_disksFiles += $VMHardDisk.ExtensionData.Backing.FileName.Replace(".vmdk","-delta.vmdk")
			$o_instanceVDisk = $VMHardDisk.ExtensionData.Backing.Parent
			while( $o_instanceVDisk ){
				$as_disksFiles += $o_instanceVDisk.FileName.Replace(".vmdk","-delta.vmdk")
				$o_instanceVDisk = $o_instanceVDisk.Parent
			}
			$as_disksFiles[($as_disksFiles.Count -1)] = ($as_disksFiles[($as_disksFiles.Count -1)]).Replace("-delta.vmdk","-flat.vmdk")
			# on va récupérer cet espace réellement pris
			$as_confVDisk.CapacityConsummed = [Int64]0
			foreach($s_diskFile in $as_disksFiles){
				$as_confVDisk.CapacityConsummed += ($VM.ExtensionData.LayoutEx.File | ?{$_.Name -ccontains $s_diskFile} ).Size
			}
			$as_confVDisk.CapacityAllocated = $VMHardDisk.CapacityKB*1024
		}	
		else{
			# si c'est un vmdk
			$as_confVDisk.CapacityAllocated = ($VM.ExtensionData.LayoutEx.File | ?{$_.Name -ccontains ($VMHardDisk.Filename.Replace(".vmdk","-flat.vmdk"))} ).Size
			$as_confVDisk.CapacityConsummed = $VMHardDisk.CapacityKB*1024
		}
		# retrouver l'ID du datastore
		$as_confVDisk.DatastoreId = $VMHardDisk.ExtensionData.Backing.Datastore
		# extraction du Datastore depuis le path
		$VMDKDatastore = ($VMHardDisk.Filename.Split(' '))[0].Trim()
		$VMDKDatastore = $VMDKDatastore.Substring(1,$VMDKDatastore.Length -2)
		$as_confVDisk.Datastore = $VMDKDatastore
	
		# suivant si c'est un RDM ou un VMDK
		if ( ($VMHardDisk.DiskType -eq "RawVirtual") -or ($VMHardDisk.DiskType -eq "RawPhysical") ) {
			$as_confVDisk.StorageFormat = ($VMHardDisk.DiskType).ToString()
			$as_confVDisk.StorageNAA = ($VMHardDisk.ScsiCanonicalName).ToString()
		}
		else{
			$as_confVDisk.StorageFormat = ($VMHardDisk.StorageFormat).ToString()
			$as_confVDisk.StorageNAA = $null
		}
		
		# ajout au tableau
		$ao_confVDisks += $as_confVDisk
	}

	# retour des arrays et du temps de traitement
	return $ao_confVMX, $ao_confVDisks, $ao_confNPIV
}





# ___________________________________________________________________________________________________________________________________________________________
# IN DEVEL --------------------------------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------------------------------------
# __________________________________________________
# récupération des configurations sur les VMs
# INPUT:
#	[VmfsDatastoreImpl]DS : objet Datastore (unique)
# OUTPUT:
#	[Array]
#		[0]$s_confDatastore
#		[2][Double]D_totalTimeTaken : temps d'exécution
# --------------------------------------------------
function Get-DSInfos{
	# paramètres
	param(
		[Parameter(Mandatory=$true,Position=1)]
		[VMware.VimAutomation.ViCore.Impl.V1.DatastoreManagement.VmfsDatastoreImpl]$DS = $(throw "Datastore object must be provided"),
		[Parameter(Mandatory=$false)]
		[String]$LogFile = "$env:TEMP\ConfGet.log"
	)
	
	# démarrage du compteur de timing
	$methStart = Get-Date

	# on renseigne les hashtable (globale) moref<>name et moref<>naa
	$global:h_DS_moref_name[$DS.Id] = $DS.Name
	# on renseigne la hashtable moref<>naa
	$DS.ExtensionData.Info.Vmfs.Extent | %{ $global:h_DS_moref_naa[$DS.Id] = $_.DiskName }

	# récupération du getview
	$o_DSGetView = $DS | Get-View

	# définition du string-tableau
	#$s_confDatastoreVMFS = "" | Select-Object Version,DatacenterId,SIOC,Capacity,FreeSpace,Name,MoRef,VMs,Hosts,MHA,Usable,Extents,BlockSize
	#$s_confDatastoreNFS = "" | Select-Object DatacenterId,SIOC,Capacity,FreeSpace,Name,MoRef,VMs,Hosts,MHA,Usable,NFSServer,SharePath
	$s_confDatastore = "" | Select-Object Type,Version,DatacenterId,SIOC,Capacity,FreeSpace,Name,MoRef,VMs,Hosts,MHA,Usable,Extents,BlockSize,NFSServer,NFSPath,ParentFolder,ParentFolderId
	
	# maintenant qu'on a le getview on ajoute toutes les properties
	$s_confDatastore.Type = $DS.Type
	$s_confDatastore.Version = $DS.FileSystemVersion
	$s_confDatastore.DatacenterId = $DS.DatacenterId
	$s_confDatastore.SIOC = $DS.StorageIOControlEnabled
	$s_confDatastore.Capacity = $DS.ExtensionData.Summary.Capacity
	$s_confDatastore.FreeSpace = $DS.ExtensionData.Summary.FreeSpace
	$s_confDatastore.Name = $DS.Name
	$s_confDatastore.MoRef = $DS.Id
	$s_confDatastore.VMs = ""
	foreach( $vmentity in $DS.ExtensionData.Vm){
		#$vmeth.IpAddress | %{ $s_VMguestip += "|" + $_.ToString() }
		$s_confDatastore.VMs += "|" + $vmentity.ToString()
		$s_confDatastore.VMs = ($s_confDatastore.VMs).TrimStart('|')
	}
	$s_confDatastore.Hosts = ""
	foreach( $hostentity in $DS.ExtensionData.Host){
		$s_confDatastore.Hosts += "|" + $hostentity.Key.ToString()
		$s_confDatastore.Hosts = ($s_confDatastore.Hosts).TrimStart('|')
	}
	$s_confDatastore.MHA = $DS.ExtensionData.Summary.MultipleHostAccess
	if( ($DS.ExtensionData.Summary.Accessible) -and ($DS.FreeSpaceMB -ge 500) -and ($DS.ExtensionData.Summary.MaintenanceMode -match "normal") -and ($DS.State -match "Available") ){
		$s_confDatastore.Usable = $true
	}
	else{
		$s_confDatastore.Usable = $false
	}
	$s_confDatastore.Capacity = $DS.ExtensionData.Summary.Capacity
	$s_confDatastore.FreeSpace = $DS.ExtensionData.Summary.FreeSpace
	$s_confDatastore.Extents = ""
	foreach( $dsextent in $DS.ExtensionData.Info.Vmfs.Extent){
		$s_confDatastore.Extents += "|" + $dsextent.DiskName.ToString()
		$s_confDatastore.Extents = ($s_confDatastore.Extents).TrimStart('|')
	}
	$s_confDatastore.BlockSize = $DS.ExtensionData.Info.Vmfs.BlockSizeMb
	$s_confDatastore.NFSServer = $DS.RemoteHost
	$s_confDatastore.NFSPath = $DS.RemotePath
	$s_confDatastore.ParentFolder = $DS.ParentFolder
	$s_confDatastore.ParentFolderId = $DS.ParentFolderId

	# fin du compteur de temps de parsing
	$methEnd = Get-Date
	$D_totalTimeTaken = ($methEnd - $methStart).TotalSeconds
	# retour des arrays et du temps de traitement
	return $s_confDatastore, $D_totalTimeTaken
}

# ___________________________________________________________________________________________________________________________________________________________
# ENDING OF DEVEL --------------------------------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------------------------------------


Export-ModuleMember -Function * -Alias *