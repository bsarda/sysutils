﻿# ----------------------------------------------------------------------------------------------------
function Log-Rotate{
	<#
	.SYNOPSIS
		fonction de rotation de fichiers log
	.DESCRIPTION
		fonction de rotation de fichiers log
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Path
		chemin du fichier log
	.PARAMETER Keep
		nombre de logs conservés
	.INPUTS
		[String] Path
		[Int16] Keep
	.OUTPUTS
		[Int] ErrorCode
		ERRORCODES:
			0 : exécution OK
			-1: erreur des arguments, mauvais format
			-2: fichier log tourner innexistant
			-3: erreur de dir
			-4: erreur de suppression du dernier fichier
			-5: erreur de déplacement d'un fichier
			-6: erreur de compression du fichier en cours
			-7: erreur de suppression du log à compresser
	.EXAMPLE
		Log-Rotate -Path "c:\event.log" -Keep 10
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Path",
		SupportsShouldProcess=$true
	)]
	param(
		[Parameter(Mandatory=$true,Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$Path,
		[Parameter(Mandatory=$false,Position=1)]
		[Int16]$Keep = 10
	)
	
	# si les paramètres ne sont pas au bon format
	if( ($Keep -isnot [Int16]) -or ($Path -isnot [String]) -or ([String]::IsNullOrEmpty($Path)) -or ($Keep -lt 0) ){
		return -1
	}
	
	# résolution en chemin absolu - le fichier doit exister
	try{
		$s_pathFullName = (Resolve-Path -Path $Path).Path
		$o_pathInfo = New-object System.IO.FileInfo($Path)
	}
	catch{
		return -2
	}
	
	# construction du filtre pour regex
	$s_regexFilter = "^(" + $o_pathInfo.Name + ")\.[0-9]*\.(zip)"
	# dir du dossier suivant filtre, ordre inverse
	try{
		$ao_filesInfoList = Get-ChildItem -Path $($o_Path.DirectoryName) | ?{$_.Attributes -ne "Directory"} | ?{$_.Name -match $s_regexFilter } | Sort -Descending
	}
	catch{
		return -3
	}

	foreach( $o_fileInfo in $ao_filesInfoList ){
		# key = $s_file et value à trouver. l'extension est .zip, donc on split les deux derniers .
		$as_filePathSplit = ($o_fileInfo.Name).Split('.')
		$i_newFileIndex = [Int]($as_filePathSplit[$as_filePathSplit.Length-2]) + 1

		# si on dépasse le Keep, on supprime
		if($i_newFileIndex -gt $Keep){
			try{
				Remove-Item -Path $o_fileInfo.FullName -Force
			}
			catch{
				return -4
			}
		}
		# déplacement des fichiers
		else{
			try{
				#[System.IO.File]::Move( $s_file.FullName, $($s_file.DirectoryName + "\" + $o_pathInfo.Name + "." + $([Int]($as_filePathSplit[$as_filePathSplit.Length-2]) + 1) + ".zip") )
				[System.IO.File]::Move( $o_fileInfo.FullName, $($o_fileInfo.DirectoryName + "\" + $o_pathInfo.Name + "." + $i_newFileIndex + ".zip") )
			}
			catch{
				return -5
			}
		}
	}
	
	# compression du fichier en cours
	$i_compressFileReturnCode = Compress-File -In $s_pathFullName -Out ($s_Path + ".0.zip") -Verbose
	if( $i_compressFileReturnCode -le 0 ){
		return -6
	}
	
	# si on arrive jusqu'ici, on peut supprimer le fichier source
	try{
		Remove-Item -Path $s_pathFullName -Confirm:$false -Force
	}
	catch{
		return -7
	}
		
	# fin correcte
	return 0
}

# ----------------------------------------------------------------------------------------------------
function Log-Create{
	<#
	.SYNOPSIS
		création fichier de log
	.DESCRIPTION
		création fichier de log
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Path
		chemin du fichier log
	.PARAMETER Keep
		nombre de logs conservés
	.INPUTS
		[String] Path
		[Int16] Keep
	.OUTPUTS
		[Int] ErrorCode
		ERRORCODES:
			0 : exécution OK
			-1: erreur des arguments, mauvais format
			-2: erreur de logrotate
			-3: erreur de création, erreur inconnue
	.EXAMPLE
		Log-Create -Path "c:\event.log" -Keep 10
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Path",
		SupportsShouldProcess=$true
	)]
	param(
		[Parameter(Mandatory=$true,Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$Path,
		[Parameter(Mandatory=$false,Position=1)]
		[Int16]$Keep = 10
	)
	
	# si les paramètres ne sont pas au bon format
	if( ($Path -isnot [String]) -or ($Keep -isnot [Int16]) -or ([String]::IsNullOrEmpty($Path)) -or ($Keep -lt 0) ){
		return -1
	}
	
	# on s'assure d'avoir le chemin complet
	$o_pathInfo = New-Object System.IO.FileInfo $Path
	$s_pathFullName = $o_pathInfo.FullName
	
	# si le fichier existe, logrotate
	if( Test-Path -Path $s_pathFullName ){
		$i_logrotate = Log-Rotate -Path $s_pathFullName -Keep $Keep
		if( $i_logrotate -lt 0 ){
			return -2
		}
	}
	
	# creation du log
	try{ 
		# creation du fichier log
		$o_fileInfocreate = New-Item -Path $s_pathFullName –ItemType File
	}
	catch [System.IO.FileNotFoundException]{
		return -2
	}
	catch{
		return -3
	}
	
	# retour correct
	return 0
}

# ----------------------------------------------------------------------------------------------------
function Log-Write{
	<#
	.SYNOPSIS
		écriture dans le fichier log d'une ligne
	.DESCRIPTION
		écriture dans le fichier log d'une ligne
		DEPENDANCES:
		Log-Create, Log-Rotate
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Path
		chemin du fichier log
	.PARAMETER Info
		texte à insérer dans le log
		sera précédé de la date (format AAAA-MM-DD HH:MM:SS, timezone UTC) et de ' -- '
	.PARAMETER Keep
		nombre de logs conservés
	.PARAMETER MaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.INPUTS
		[String] Path 
		[String] Info
		[Int16] Keep
		[Int] MaxSize
	.OUTPUTS
		[System.Int] ErrorCode
		ERRORCODES:
			0 : exécution OK
			-1: erreur des arguments, mauvais format
			-2: creation du fichier log echouée
			-3: fichier log à écrire innexistant
			-4: logrotate echoué
			-5: creation du fichier log après logrotate echouée
			-6: ajout de la ligne échouée
	.EXAMPLE
		Log-Write -Path "c:\event.log" -Keep 10 -MaxSize 1024 -Info "[INFO] Entrée du journal"
	.LINK
		b.sarda@free.fr
	#>

	[CmdletBinding(
		DefaultParameterSetName="Path",
		SupportsShouldProcess=$true
	)]
	param(
		[Parameter(Mandatory=$true,Position=0)]
		[String]$Path = $(throw "Path to logfile must be provided"),
		[Parameter(Mandatory=$true,Position=1,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$Info,
		[Parameter(Mandatory=$false,Position=2)]
		[Int16]$Keep = 10,
		[Parameter(Mandatory=$false,Position=3)]
		[Int]$MaxSize = 10240
	)
	
	# si les paramètres ne sont pas au bon format
	if( ($Path -isnot [String]) -or ($Info -isnot [String]) -or ([String]::IsNullOrEmpty($Info)) -or ($Keep -isnot [Int16]) -or ($Keep -lt 1) -or ($MaxSize -isnot [Int]) -or ($MaxSize -lt 128) ){
		return -1
	}
	
	# test d'existence du fichier log
	if(!(Test-Path -Path $Path ) ){
		$i_logCreateReturnCode = Log-Create -Path $Path -Keep $Keep
		if( $i_logCreateReturnCode -lt 0 ){
			return -2
		}
	}
	
	# résolution en chemin absolu - le fichier doit exister
	try{
		$s_pathFullName = (Resolve-Path -Path $Path).Path
	}
	catch{
		return -3
	}
		
	# si taille max atteinte, rotate et nouveau fichier
	$o_pathInfo = New-Object System.IO.FileInfo($s_pathFullName)
	if( ($o_Path.Length)/1024 -ge $MaxSize ){
		$i_logrotate = Log-Rotate -Path $s_pathFullName -Keep $Keep
		$i_logCreateReturnCode = Log-Create -Path $s_pathFullName -Keep $Keep
		if( $i_logrotate -lt 0 ){
			return -4
		}
		if( $i_logCreateReturnCode -lt 0 ){
			return -5
		}
	}
	
	# ajout de la ligne du paramètre
	try{
		Add-Content -Path $s_pathFullName -Value ( (Get-Date -Date ((Get-Date).ToUniversalTime()) -Format u) + " -- " + $Info)
	}
	catch{
		return -6
	}

	# retour correct
	return 0
}

Export-ModuleMember -Function * -Alias *