﻿# ----------------------------------------------------------------------------------------------------
function Test-Connectivity{
	<#
	.SYNOPSIS
		test de connectivité sur un host, port tcp
	.DESCRIPTION
		test de connectivité sur un host, port tcp
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Server
		nom du serveur (fqdn ou IP)
	.PARAMETER Port
		numéro de port à tester
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[String] Server
		[Int16] Port
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[Int] ErrorCode
		ERRORCODES:
			>0 : exécution OK (RTT pour un test ping, port local pour un test socket)
			-1: erreur des arguments, mauvais format
			-2: ping failed
			-3: exception lors du send ping
			-4: socket ouverte mais non connectée (timeout? tcpwindow?)
			-5: port cible non disponible
	.EXAMPLE
		Test-Connectivity -Server 10.10.10.1 -Port 443
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Server",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(Mandatory=$true,Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$Server,
		[Parameter(Mandatory=$false)]
		[Int]$Port = 0,
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)

	if( ($Server -isnot [String]) -or ([String]::IsNullOrEmpty($Server)) -or ($Port -isnot [Int])  ){
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect parameters"
		}
		return -1
	}

	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [PROGRESS] The function '$($MyInvocation.InvocationName)' is running"
	}

	# si le port n'est pas renseigné, test de ping classique
	if( $Port -eq 0 ){
		try{
			$o_networkPing = New-Object System.Net.NetworkInformation.Ping
			$o_networkPingReply = $o_networkPing.Send($Server)
			if ($o_networkPingReply.Status –eq “Success”){
				# ajout d'une entrée dans le log, si fourni
				if( !([String]::IsNullOrEmpty($LogFile)) ){
					$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] ping successfully server '$Server', RTT=$o_networkPingReply.RoundtripTime seconds"
				}
				return $o_networkPingReply.RoundtripTime
			}
			else{
				# ajout d'une entrée dans le log, si fourni
				if( !([String]::IsNullOrEmpty($LogFile)) ){
					$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] ping failed to server '$Server'"
				}
				return -2
			}
		}
		catch{
			# ajout d'une entrée dans le log, si fourni
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Unable to create ping request"
			}
			return -3
		}
	}
	# sinon, test de port
	else{
		try{
			# ajout d'une entrée dans le log, si fourni
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Testing socket creation on $Server : $Port"
			}
			$i_returnCode = -4
			$o_tcpSocket = New-Object System.Net.Sockets.TcpClient($Server, $Port)
			if( $o_tcpSocket.Connected -eq $true ){
				$i_returnCode = $o_tcpSocket.Client.LocalEndPoint.Port
				# ajout d'une entrée dans le log, si fourni
				if( !([String]::IsNullOrEmpty($LogFile)) ){
					$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Socket to $Server : $Port successfully created"
				}
			}
			$o_tcpSocket.Close()
			return $i_returnCode
		}
		catch{
			# ajout d'une entrée dans le log, si fourni
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Unable to open socket to $Server : $Port"
			}
			return -5
		}
	}
}

Export-ModuleMember -Function * -Alias *