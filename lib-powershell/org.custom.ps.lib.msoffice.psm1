# ----------------------------------------------------------------------------------------------------
function Excel-AddSheetsFromCsv{
	<#
	.SYNOPSIS
		ajout d'un sheet depuis un csv
	.DESCRIPTION
		ajout d'un sheet depuis un csv
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Excel
		nom du fichier excel (chemin complet)
	.PARAMETER CsvFiles
		nom du fichier csv (chemin complet)
	.PARAMETER Update
		mise à jour ou ecrasement du excel
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[String] Excel
		[Array] CsvFiles
		[Bool] Update
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[Int] ErrorCode
		ERRORCODES:
			1 : exécution OK
			0
			-1: erreur des arguments, mauvais format
			-2: fichier déjà existant et pas d'update désiré
			-3: objet com non créé
			-4: erreur de lecture de l'Excel (pour Update)
			-5: erreur d'ouverture du CSV
			-6: erreur du copier
	.EXAMPLE
		Excel-AddSheetsFromCsv -Excel c:\test.xls -CsvFiles (Get-ChildItem -Path "C:\exportMetrics\2013-10-21" -Filter "*.csv") -Update $false
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Excel",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(Mandatory=$true,Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$Excel,
		[Parameter(Mandatory=$true,Position=1)]
		[Array]$CsvFiles,
		[Parameter(Mandatory=$false)]
		[Bool]$Update = $true,
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)

	if( ($Excel -isnot [String]) -or ([String]::IsNullOrEmpty($Excel)) ){
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect parameters"
		}
		return -1
	}

	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [PROGRESS] The function '$($MyInvocation.InvocationName)' is running"
	}

	# test d'existence du fichier
	if( ((Test-Path -Path $Excel) -eq $true) -and (!($Update))){
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] The file '$Excel' already exists"
		}
    	return -2
  	}

	# creation de l'objet COM
	$o_ComExcel = New-Object -Com Excel.Application
	if( !($o_ComExcel) ){
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Exception while creating COM object"
		}
    	return -3
  	}

	# ne pas montrer excel et supprimer les alertes
	$o_ComExcel.Visible = $False
	$o_ComExcel.DisplayAlerts = $False
	if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Excel launched, not visible"
	}

	# si c'est une création du fichier
	if( !($Update) ){
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Creating from scratch"
		}
		# ajout du workbook
		$o_ExcelWorkbook = $o_ComExcel.Workbooks.Add()
		
		# suppression des feuilles supplémentaires
		$o_ExcelWorkbook.Worksheets.Item(2).Delete()
		$o_ExcelWorkbook.Worksheets.Item(2).Delete()
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Sheets 2 and 3 deleted"
		}
		# pour chaque fichier dans la liste
		ForEach($Csv in $CsvFiles){
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Processing CSV file '$($Csv.BaseName)'"
			}
			# ajout d'une feuille
			$o_ExcelWorkbook.Worksheets.Add() | Out-Null
			# utilisation de la dernière feuille créée (forcément la 1)
			$o_WorkSheet = $o_ExcelWorkbook.Worksheets.Item(1)
			# nommage de la feuille
			$o_WorkSheet.Name = $Csv.BaseName
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Sheet added, selected and named"
			}
			
			
			# ouverture du csv dans excel
			$o_tempCsv = $o_ComExcel.Workbooks.Open($Csv.FullName)
			if( [String]::IsNullOrEmpty($o_tempCsv.FullName) ){
				if( !([String]::IsNullOrEmpty($LogFile)) ){
					$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Cant open CSV file"
				}
				return -5
			}
			# selection de la feuille active
			$o_tempSheetFromCsv = $o_tempCsv.Worksheets.Item(1)
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] CSV file opened, start copy and paste"
			}

			# copie des contenus du CSV
			if( !($o_tempSheetFromCsv.UsedRange.Copy()) ){
				if( !([String]::IsNullOrEmpty($LogFile)) ){
					$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Cant copy content"
				}
				return -6
			}
			# collage des contenus sur Excel
			$o_WorkSheet.Paste()
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Content from CSV pasted"
			}

			# fermeture du csv source
			$o_tempCsv.Close()
		}
	}
	else{
		# ajout du workbook
		$o_ExcelWorkbook = $o_ComExcel.Workbooks.Open($Excel)
		if( $o_ExcelWorkbook.ReadOnly ){
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Cant open Excel file in read/write"
			}
			return -4
		}
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Excel file opened for update"
		}
		
		# dans le cas d'un update
		# pour chaque fichier dans la liste
		ForEach($Csv in $CsvFiles){
			# nom de la feuille telle qu'elle doit être
			$s_SheetName = $Csv.BaseName
			# retrouver la feuille correspondante
			$o_ActiveWS = $o_ExcelWorkbook.Worksheets | ?{$_.Name -eq $s_SheetName}
			$o_WorkSheet = $o_ExcelWorkbook.Worksheets.Item($s_SheetName)
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Processing CSV file '$($Csv.BaseName)'"
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Active Excel sheet '$($o_ActiveWS.Name)'"
			}

			# ouverture du csv dans excel
			$o_tempCsv = $o_ComExcel.Workbooks.Open($Csv.FullName)
			if( [String]::IsNullOrEmpty($o_tempCsv.FullName) ){
				if( !([String]::IsNullOrEmpty($LogFile)) ){
					$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Cant open CSV file"
				}
				return -5
			}
			# selection de la feuille active
			$o_tempSheetFromCsv = $o_tempCsv.Worksheets.Item(1)
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] CSV file opened, start copy and paste"
			}
			
			# copie des contenus du CSV
			if( !($o_tempSheetFromCsv.UsedRange.Copy()) ){
				if( !([String]::IsNullOrEmpty($LogFile)) ){
					$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Cant copy content"
				}
				return -6
			}
			# collage des contenus sur Excel
			$o_WorkSheet.Paste()
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Content from CSV pasted"
			}

			# fermeture du csv source
			$o_tempCsv.Close()
		}
	}
	# sauvegarde du workbook
	$o_ExcelWorkbook.RefreshAll()
	$o_ExcelWorkbook.SaveAs($Excel)
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Excel saved as '$Excel'"
	}
	# fermeture d'excel
	$o_ComExcel.Quit()
}

Export-ModuleMember -Function * -Alias *
