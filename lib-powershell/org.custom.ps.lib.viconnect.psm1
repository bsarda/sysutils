# ----------------------------------------------------------------------------------------------------
Function New-XMLCfgFileVI{
	<#
	.SYNOPSIS
		création de fichier de connexion à un serveur vcenter, en mode crypté
	.DESCRIPTION
		création de fichier de connexion à un serveur vcenter, en mode crypté
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Path
		chemin du fichier xml à créer (juste le dossier)
	.PARAMETER Server
		adresse ip ou fqdn du serveur vCenter Server (celui ou se trouve le SDK, port 443 par défaut)
	.PARAMETER Login
		login sur le vcenter server
	.PARAMETER Password
		mot de passe associé, en System.Security.SecureString
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[String] Path
		[String] Server
		[String] Login
		[System.Security.SecureString] Password
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[Int] ErrorCode
		ERRORCODES:
			>0 : exécution OK, taille du fichier XML (int64)
			-1: erreur des arguments, mauvais format
			-2: erreur de cryptage des variables
			-3: erreur création de l'objet pour population avant export
			-4: export xml impossible
	.EXAMPLE
		New-XMLCfgFileVI -Path "c:\scripts\cfg" -Server "10.10.10.1 -Login "admin" -Password [System.Security.SecureString]
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Path",
		SupportsShouldProcess=$true
	)]
	param(
		[Parameter(Mandatory=$true)]
		[String]$Path,
		[String]$Server,
		[String]$Login,
		[System.Security.SecureString]$Password,
		[Parameter(Mandatory=$false)]
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)
	
	# si les paramètres ne sont pas au bon format
	if( ($Path -isnot [String]) -or ($Server -isnot [String]) -or ($Login -isnot [String]) -or ($Password -isnot [System.Security.SecureString]) ){
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect parameters"
		}
		return -1
	}
	
	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [PROGRESS] The function '$($MyInvocation.InvocationName)' is running"
	}
	
	# clé de cryptage 32 octets
	$ai_encryptionKey = (181,86,168,160,21,85,16,8,70,133,104,209,33,54,245,50,162,62,144,220,123,129,99,246,4,199,20,162,112,52,190,47)

	# cryptage des chaines
	try{
		$ss_viServer = ( $Server | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString -Key $ai_encryptionKey )
		$ss_viLogin = ( $Login | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString -Key $ai_encryptionKey )
		$ss_viPass = ( $Password | ConvertFrom-SecureString -Key $ai_encryptionKey )
	}
	catch{
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Unexpected error while converting input to encrypted SecureString"
		}
		return -2
	}

	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Successfully converted input to encrypted SecureString"
	}

	# création de l'objet
	try{
		$o_xmlCfgFile = New-Object System.Object
    	$o_xmlCfgFile | Add-Member -MemberType NoteProperty -name VIServer -value $ss_viServer
		$o_xmlCfgFile | Add-Member -MemberType NoteProperty -name VILogin -value $ss_viLogin
		$o_xmlCfgFile | Add-Member -MemberType NoteProperty -name VIPass -value $ss_viPass
	}
	catch{
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Unable to create PSObject which contains the encrypted SecureStings"
		}
		return -3
	}
	
	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] PSObject which contains the encrypted SecureStings successfully created"
	}
	# puis export en XML
	try{
		$o_xmlCfgFile | Export-Clixml -Path "$Path\$Server.xml"
		$o_xmlCfgFileInfo = New-Object System.IO.FileInfo("$Path\$Server.xml")
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] XML file '$Path\$Server.xml' successfully created"
		}
		# retour correct
		Return $o_xmlCfgFileInfo.Length
	}
	catch{
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Unable to create the XML file '$Path\$Server.xml'"
		}
		return -4
	}
}

# ----------------------------------------------------------------------------------------------------
Function Get-XMLCfgFileVI{
<#
	.SYNOPSIS
		lecture des infos de connexion depuis XML crypté
	.DESCRIPTION
		lecture des infos de connexion depuis XML crypté
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Path
		chemin jusqu'au ficher xml, ie cfg\vcenter51.xml
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[String] Path
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[String] Server : FQDN ou IP du serveur vCenter
		[String] Login : nom d'utilisateur de connection
		[System.Security.SecureString] Password : mot de passe de connexion
		
		[Int] ErrorCode
		ERRORCODES:
			-1: erreur des arguments, mauvais format
			-2: fichier XML introuvable
			-3: erreur d'import du xml en objet
			-4: erreur de conversion en SecureSting / de récupération des objets
			-5: erreur de décryptage
	.EXAMPLE
		Get-XMLCfgFileVI -Path "c:\scripts\cfg\vcenter51.xml"
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Path",
		SupportsShouldProcess=$true
	)]
	param(
		[Parameter(Mandatory=$true,Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$Path,
		[Parameter(Mandatory=$false)]
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)
	
	# si les paramètres ne sont pas au bon format
	if( ($Path -isnot [String]) -or ([String]::IsNullOrEmpty($Path)) ){
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect 'Path' parameter"
		}
		return -1
	}
	
	# clé de cryptage 32 octets
	$ai_encryptionKey = (181,86,168,160,21,85,16,8,70,133,104,209,33,54,245,50,162,62,144,220,123,129,99,246,4,199,20,162,112,52,190,47)

	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [PROGRESS] The function '$($MyInvocation.InvocationName)' is running"
	}
	
	# lecture du XML
 	try{
		$o_importedXml = Import-Clixml -Path $Path
	}
	catch [System.IO.FileNotFoundException]{
		# cas de l'erreur de fichier introuvable
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] XML config file '$Path': unable to read"
		}
		return -2
	}
	catch{
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Unexpected error while reading XML config file '$Path'"
		}
		return -3
	}
	
	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] XML config file '$Path' successfully read"
	}
	
	# passage du contenu
	try{
		# lecture de chaque champ, normalement présent, et conversion en securestring.
		$ss_VIServer = $o_importedXml.VIServer | ConvertTo-SecureString -Key $ai_encryptionKey
		$ss_VILogin = $o_importedXml.VILogin | ConvertTo-SecureString -Key $ai_encryptionKey
		$ss_VIPass = $o_importedXml.VIPass | ConvertTo-SecureString -Key $ai_encryptionKey
	}
	catch{
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Unexpected error while parsing file '$Path'. Please check file compliance"
		}
		return -4
	}
	
	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Parsing file '$Path' successful"
	}

	# décryptage des valeurs
	try{
		# décryptage du vcenter (copie SS vers blocs mémoire > conversion pointeur vers string > libération mémoire)
		$ptr_SecureStringToMemoryBloc = [System.Runtime.InteropServices.Marshal]::SecureStringToCoTaskMemUnicode($ss_VIServer)
		$s_VIServer = [System.Runtime.InteropServices.Marshal]::PtrToStringUni($ptr_SecureStringToMemoryBloc)
		[System.Runtime.InteropServices.Marshal]::ZeroFreeCoTaskMemUnicode($ptr_SecureStringToMemoryBloc)
		# décryptage du login (copie SS vers blocs mémoire > conversion pointeur vers string > libération mémoire)
		$ptr_SecureStringToMemoryBloc = [System.Runtime.InteropServices.Marshal]::SecureStringToCoTaskMemUnicode($ss_VILogin)
		$s_VILogin = [System.Runtime.InteropServices.Marshal]::PtrToStringUni($ptr_SecureStringToMemoryBloc)
		[System.Runtime.InteropServices.Marshal]::ZeroFreeCoTaskMemUnicode($ptr_SecureStringToMemoryBloc)
		# décryptage du mot de passe / inutile sauf pour débug
		#$ptr_SecureStringToMemoryBloc = [System.Runtime.InteropServices.Marshal]::SecureStringToCoTaskMemUnicode($ss_VIPass)
		#$s_VIPass = [System.Runtime.InteropServices.Marshal]::PtrToStringUni($ptr_SecureStringToMemoryBloc)
		#[System.Runtime.InteropServices.Marshal]::ZeroFreeCoTaskMemUnicode($ptr_SecureStringToMemoryBloc)
	}
	catch{
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Unexpected error while decrypting XML SecureString"
		}
		return -5
	}
	
	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] decrypt of XML content successful"
	}
	
	# retour des arrays et du temps de traitement
	return $s_VIServer, $s_VILogin, $ss_VIPass
}

# ----------------------------------------------------------------------------------------------------
function VIConnect{
	<#
	.SYNOPSIS
		connexion au vCenter Server
	.DESCRIPTION
		connexion au vCenter Server
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Server
		serveur vCenter Server à joindre (FQDN ou IP)
	.PARAMETER Port
		port du serveur vCenter si pas par défaut (443)
	.PARAMETER Login
		login de connexion, type domain\user
	.PARAMETER Password
		mot de passe associé, en System.Security.SecureString
	.PARAMETER Port
		port de connexion du vCenter Server (par défaut 443)
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[String] Server
		[String] Login
		[SecureString] Password
		[String] File
		[Int] Port
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[VIServerImpl] o_viconnex : serveur vcenter connecté
		
		[Int] ErrorCode
		ERRORCODES:
			-1: erreur des arguments, non fournis
			-2: erreur des arguments, mauvais format
			-3: ne peut atteindre server/port
			-4: connexion impossible
			-5: erreur inconnue durant l'init de la connexion
	.EXAMPLE
		VIConnect -Server "vcenter51" -Login "domain\admin" -Password $password
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Path",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(Mandatory=$false,Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$File,
		
		[Parameter(Mandatory=$false,Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$Server,
		[Parameter(Mandatory=$false,Position=1)]
		[String]$Login,
		[Parameter(Mandatory=$false,Position=2)]
		[System.Security.SecureString]$Password,
		
		[Parameter(Mandatory=$false)]
		[Int]$Port = 443,
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)

	# fournir l'un ou l'autre
	if( ($Server -isnot [String]) -or ($File -isnot [String]) ){
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] You must enter Server or File parameters"
		}
		return -1
	}
	
	# si les paramètres ne sont pas au bon format
	if( !([String]::IsNullOrEmpty($File)) -and ($File -is [String]) ){
		$o_File = New-Object System.IO.FileInfo($File)
		if( !($o_File.Exists) ){
			# ajout d'une entrée dans le log, si fourni
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] You must enter Server or File parameters"
			}
			return -2
		}
	}
	elseif( !([String]::IsNullOrEmpty($Server)) -and ($Server -is [String]) ){
		if( ($Login -isnot [String]) -or ($Password -isnot [System.Security.SecureString])){
			# ajout d'une entrée dans le log, si fourni
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Server/Login/Pass : Incorrect parameter"
			}
			return -2
		}
	}
	else{
		# ni l'un ni l'autre, erreur d'arguments
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] You must enter Server or File parameters"
		}
		return -1
	}
	
	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [PROGRESS] The function '$($MyInvocation.InvocationName)' is running"
	}
	
	try{
		# si on a fourni le fichier XML, il faut le lire
		if( (!([String]::IsNullOrEmpty( $File ))) ){
			if( $LogFile ){
				$as_cfgFile = Get-XMLCfgFileVI -Path $File -LogFile $LogFile
			}
			else{
				$as_cfgFile = Get-XMLCfgFileVI -Path $File
			}
			# puis renseigner les variables
			$Server = $as_cfgFile[0]
			$Login = $as_cfgFile[1]
			$Password = $as_cfgFile[2]
		}
		# test de connexion
		$i_rtt = Test-Connectivity -Server $Server -Port $Port
		if( $i_rtt -le 0 ){
			# ajout d'une entrée dans le log, si fourni
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Cannot reach server port"
			}
			return -3
		}
		# connexion au vcenter server
		$o_viSecureCredentials = New-Object System.Management.Automation.PSCredential -ArgumentList $Login,$Password
		$o_viServerConnexion = Connect-VIServer -Server $Server -Credential $o_viSecureCredentials -Port $Port -ErrorAction SilentlyContinue
		
		if(!($o_viServerConnexion.IsConnected)){
			# ajout d'une entrée dans le log, si fourni
			if( !([String]::IsNullOrEmpty($LogFile)) ){
				$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Unable to connect to vCenter Server: '$Server' ; port: $Port"
			}
			return -4
		}
		
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Successfully connected to vCenter Server: '$Server'"
		}
		# retour des arrays et du temps de traitement
		return $o_viServerConnexion
	}
	catch{
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Unknow error while connecting to vCenter Server: '$Server'"
		}
		return -5
	}
}

# ----------------------------------------------------------------------------------------------------
function VIDisconnect{
	<#
	.SYNOPSIS
		déconnexion du vCenter Server
	.DESCRIPTION
		déconnexion du vCenter Server
	.NOTES
		Author: Benoit Sarda
		Date: july 2013
	.PARAMETER Server
		nom/ip du serveur duquel se déconnecter
		peut être un objet VIServer
	.PARAMETER LogFile
		chemin du fichier log
	.PARAMETER LogMaxSize
		taille maximale du fichier log en ko, par défaut 10Mo (mini 128k)
	.PARAMETER LogKeep
		nombre de logs conservés
	.INPUTS
		[String] Server
		[String] LogFile
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[Int] ErrorCode
		ERRORCODES:
			1 : exécution OK
			-1: erreur des arguments, mauvais format
			-2: erreur inconnue durant l'init de la déconnexion
	.EXAMPLE
		VIDisconnect -Server "vcenter51"
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Path",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(ParameterSetName="p1",Position=0,Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[VMware.VimAutomation.ViCore.Util10.VersionedObjectImpl]$ServerObj,
		[Parameter(ParameterSetName="p2",Position=0,Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$ServerName,
		[Parameter(Mandatory=$false)]
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)

	# si les paramètres ne sont pas au bon format
	#if( ($ServerName -isnot [String]) -and ($ServerObj -isnot [VMware.VimAutomation.ViCore.Util10.VersionedObjectImpl]) ){
	#	# ajout d'une entrée dans le log, si fourni
	#	if( !([String]::IsNullOrEmpty($LogFile)) ){
	#		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Incorrect parameter"
	#	}
	#	return -1
	#}
	
	# début de chono des collectes
	$methStart = Get-Date
	
	# la variable Server est globale
	if( $ServerObj -is [VMware.VimAutomation.ViCore.Util10.VersionedObjectImpl] ){
		$Server = $ServerObj
	}
	elseif($ServerName -is [String]){
		$Server = $ServerName
	}
	else{
		# si les paramètres ne sont pas au bon format
		return -1
	}
	
	# ajout d'une entrée dans le log, si fourni
	if( !([String]::IsNullOrEmpty($LogFile)) ){
		$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [PROGRESS] The function '$($MyInvocation.InvocationName)' is running"
	}
	
	try{
		# déconnexion du vcenter server
		Disconnect-VIServer -Server $Server -ErrorAction SilentlyContinue -Confirm:$false
		# fin de chono des collectes
		$methEnd = Get-Date
		$d_totalTimeTaken = ($methEnd - $methStart).TotalSeconds
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [INFO] Successfully disconnected from vCenter Server"
		}
		# retour normal
		return 1
	}
	catch{
		# ajout d'une entrée dans le log, si fourni
		if( !([String]::IsNullOrEmpty($LogFile)) ){
			$null = Log-Write -Path $LogFile -Keep $LogKeep -MaxSize $LogMaxSize -Info "[$($MyInvocation.InvocationName)] -- [ERROR] Unknow error while disconnecting from vCenter Server"
		}
		return -2
	}
}

Export-ModuleMember -Function * -Alias *