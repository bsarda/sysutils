#!/bin/bash
counter=1
while [ true ] ; do
  #echo "Name of the file is scan-${counter}.jpg"
  scanimage --format jpeg --scan-area ISO/A4/Portrait --resolution 300 > scan-${counter}.jpg
  ((counter++))
  read -p "Press any key to continue..." -n1 -s
done
