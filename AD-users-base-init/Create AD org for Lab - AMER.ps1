# generic password for all users
$genericPass = ConvertTo-SecureString -AsPlainText -Force -String 'VMware1!'
# csv input, if not provided
$CSV_RNDUSERS_AMER = "adUsersGenerated_AMER.csv"
$minGroupsCount = 4
$maxGroupsCount = 29

function Create-ADOrganizationForLab-AMER
<#
	.SYNOPSIS
		donne le niveau de stats collect du vCenter Server
	.DESCRIPTION
		donne le niveau de stats collect du vCenter Server
	.NOTES
		Author: Benoit Sarda
		Date: may 2017
	.PARAMETER csv
		CSV file for user names and informations
	.PARAMETER ADServer
		AD Server on which to run the commands
	.INPUTS
		[String] csv
		[String] ADServer
		[Int] LogMaxSize
		[Int16] LogKeep
	.OUTPUTS
		[Int] ErrorCode
		ERRORCODES:
			-1: bad arguments
	.EXAMPLE
		Create-ADOrganizationForLab-AMER
	.LINK
		b.sarda@free.fr
	#>
	
	[CmdletBinding(
		DefaultParameterSetName="Server",
		SupportsShouldProcess=$true
	)]

	# paramètres
	param(
		[Parameter(Position=0,Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String]$Server,
		[String]$csv,
		[Parameter(Mandatory=$false)]
		[String]$LogFile,
		[Int]$LogMaxSize = 10240,
		[Int16]$LogKeep = 10
	)


# domain AMER
Invoke-Command -FilePath c:\scripts\test.ps1 -ComputerName Server01
$ADServer = "172.20.1.32"
$Login = "emea\administrator"
$Password = 'V96T68d3zXFj#VD5'
$ADPassword = ConvertTo-SecureString -String $Password -AsPlainText -Force
$creds = New-Object System.Management.Automation.PSCredential -ArgumentList $Login,$ADPassword
Invoke-Command -ComputerName $ADServer -Credential $creds -ScriptBlock {
New-ADOrganizationalUnit -Name "TestOU" -Path "DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "TestV2" -Path "DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
}




New-ADOrganizationalUnit -Name "DomainUsers" -Path "DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "DomainServers" -Path "DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "DomainDesktops" -Path "DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

New-ADOrganizationalUnit -Name "US- Chicago" -Path "OU=DomainDesktops,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "US- New York" -Path "OU=DomainDesktops,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Building A" -Path "OU=US- New York,OU=DomainDesktops,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Building B" -Path "OU=US- New York,OU=DomainDesktops,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "US- San Francisco" -Path "OU=DomainDesktops,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "North" -Path "OU=US- San Francisco,OU=DomainDesktops,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "South" -Path "OU=US- San Francisco,OU=DomainDesktops,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "US- Los Angeles" -Path "OU=DomainDesktops,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CA- Montreal" -Path "OU=DomainDesktops,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CA- Ottawa" -Path "OU=DomainDesktops,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "BR- Sao Polo" -Path "OU=DomainDesktops,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

New-ADOrganizationalUnit -Name "US- Washington" -Path "OU=DomainServers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "US- Los Angeles" -Path "OU=DomainServers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Room 1" -Path "OU=US- Los Angeles,OU=DomainServers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Room 2" -Path "OU=US- Los Angeles,OU=DomainServers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CA- Montreal" -Path "OU=DomainServers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "BR- Brasilia" -Path "OU=DomainServers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

New-ADOrganizationalUnit -Name "Others" -Path "OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "US- Chicago" -Path "OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Marketting" -Path "OU=US- Chicago,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "US- New York" -Path "OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "DevOps" -Path "OU=US- New York,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Financial" -Path "OU=US- New York,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "HR" -Path "OU=US- New York,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Marketting" -Path "OU=US- New York,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "US- San Francisco" -Path "OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "DevOps" -Path "OU=US- San Francisco,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "IT Helpdesk" -Path "OU=US- San Francisco,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Marketting" -Path "OU=US- San Francisco,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "US- Los Angeles" -Path "OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "DSI" -Path "OU=US- Los Angeles,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Financial" -Path "OU=US- Los Angeles,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "HR" -Path "OU=US- Los Angeles,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CA- Ottawa" -Path "OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Financial" -Path "OU=CA- Ottawa,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "HR" -Path "OU=CA- Ottawa,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CA- Montreal" -Path "OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Marketting" -Path "OU=CA- Montreal,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Q&A" -Path "OU=CA- Montreal,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "BR- Sao Polo" -Path "OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Financial" -Path "OU=BR- Sao Polo,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "HR" -Path "OU=BR- Sao Polo,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Marketting" -Path "OU=BR- Sao Polo,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Legal" -Path "OU=BR- Sao Polo,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Validation" -Path "OU=BR- Sao Polo,OU=DomainUsers,DC=amer,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

# arrays of sites by country
$sitesUS =@("OU=Marketting,OU=US- Chicago,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=DevOps,OU=US- New York,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=Financial,OU=US- New York,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=HR,OU=US- New York,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=Marketting,OU=US- New York,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=DevOps,OU=US- San Francisco,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=IT Helpdesk,OU=US- San Francisco,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=Marketting,OU=US- San Francisco,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=DSI,OU=US- Los Angeles,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=Financial,OU=US- Los Angeles,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=HR,OU=US- Los Angeles,OU=DomainUsers,DC=amer,DC=corp,DC=local"
			)
$sitesCA =@("OU=Financial,OU=CA- Ottawa,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=HR,OU=CA- Ottawa,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=Marketting,OU=CA- Montreal,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=Q&A,OU=CA- Montreal,OU=DomainUsers,DC=amer,DC=corp,DC=local"
			)
$sitesBR =@("OU=Financial,OU=BR- Sao Polo,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=HR,OU=BR- Sao Polo,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=Marketting,OU=BR- Sao Polo,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=Legal,OU=BR- Sao Polo,OU=DomainUsers,DC=amer,DC=corp,DC=local",
			"OU=Validation,OU=BR- Sao Polo,OU=DomainUsers,DC=amer,DC=corp,DC=local"
			)
$siteDefault = "OU=Others,OU=DomainUsers,DC=amer,DC=corp,DC=local"

$sitesAll = $sitesUS + $sitesCA + $sitesBR

# --------------------------------
# create all users
$null=Import-Csv $CSV_RNDUSERS_AMER -OutVariable usersAmer
ForEach($userAmer in $usersAmer){
	# get the country 2-letter code
	if($userAmer.CountryFull -like "United States"){
		$ouPath = $sitesUS[(Get-Random -Minimum 0 -Maximum $sitesUS.Count)]
		$country = "US"
	}
	elseif($userAmer.CountryFull -like "Canada"){
		$ouPath = $sitesCA[(Get-Random -Minimum 0 -Maximum $sitesCA.Count)]
		$country = "CA"
	}
	elseif($userAmer.CountryFull -like "Brazil"){
		$ouPath = $sitesBR[(Get-Random -Minimum 0 -Maximum $sitesBR.Count)]
		$country = "BR"
	}
	else{
		$ouPath = $siteDefault;
		$country = "FR"
	}
	
	# get the correct upn
	$surnameNoAccents = [Text.Encoding]::ASCII.GetString([Text.Encoding]::GetEncoding(1251).GetBytes($userAmer.Surname))
	$pattern = '[^a-zA-Z]'
	$surnameNoSpecials = $surnameNoAccents -replace  $pattern,""
	if(($surnameNoSpecials.Length) -lt 18){
		$upnBase = "$($userAmer.GivenName.Substring(0,1).ToLower())$($surnameNoSpecials.ToLower())"
	}
	else{
		$upnBase = "$($userAmer.GivenName.Substring(0,1).ToLower())$($surnameNoSpecials.Substring(0,18).ToLower())"
	}
	# try to find if exists
	$counterUpn = 1
	$upnBaseCorrected = $upnBase
	$continueLoop = $true
	while($continueLoop){
		try{
			$null=Get-ADUser -Identity $upnBaseCorrected
		}
		catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] {
			break
		}
		$upnBaseCorrected = $upnBase+$counterUpn
		$counterUpn += 1
	}
	# infos
	Write-Host "Will create user '$upnBaseCorrected' in country $country and OU path '$ouPath'"

	# create the user
	New-ADUser -Name "$($userAmer.GivenName) $($userAmer.Surname)" -GivenName $userAmer.GivenName -Surname $userAmer.Surname`
				-DisplayName "$($userAmer.GivenName) $($userAmer.Surname)" `
				-AccountPassword $genericPass -CannotChangePassword $false -ChangePasswordAtLogon $false -PasswordNeverExpires $true `
				-StreetAddress $userAmer.StreetAddress -PostalCode $userAmer.ZipCode -City $userAmer.City -Country $country `
				-MobilePhone $userAmer.TelephoneNumber -Company $userAmer.Company `
				-EmailAddress "$upnBaseCorrected@amer.corp.com" -Organization "Corp" `
				-SamAccountName $upnBaseCorrected -UserPrincipalName "$upnBaseCorrected@amer.corp.local" -Path $ouPath -Enabled $true
}

# --------------------------------
# create all the groups
foreach($site in $sitesAll){
	# get all users
	$usersOfOU = Get-ADUser -SearchBase $site -Filter *
	# generate a part of groups names and count
	$numberOfGroups = Get-Random -Minimum $minGroupsCount -Maximum $maxGroupsCount
	$groupBase = ($site -replace '^OU=|,.*$','')
	$groupCountry = ($site -replace '^OU=.*?,OU=|,.*$','')
	$groupNamed = "G_SG__"+ ($groupBase -replace '\s','-') +'_'+ ($groupCountry -replace '\s','')
	# get group ou
	$groupPath = $site -replace "^OU=$groupBase,",''
	# for each group that might be created, create
	1..$numberOfGroups | %{
		$indexesOfUsersCandidate = Get-Random -Minimum 0 -Maximum $usersOfOU.Count
		$group = $groupNamed +"__"+ $_
		# populate future group users
		$usersCandidateList = New-Object System.Collections.Generic.List[System.Object]
		1..$indexesOfUsersCandidate | %{ $usersCandidateList.Add($usersOfOU[$_]) }
		$usersCandidate = $usersCandidateList.ToArray()
		# when we have the candidates, create the group
		$generatedGroup = New-ADGroup -Name $group -DisplayName $group -SamAccountName $group `
							-GroupCategory "Security" -GroupScope "Global" `
							-Path $groupPath -PassThru
		# and push them to it
		$generatedGroup | Add-ADGroupMember -Members $usersCandidate
	}
}
