# generic password for all users
$genericPass = ConvertTo-SecureString -AsPlainText -Force -String 'VMware1!'
# csv input, if not provided
$CSV_RNDUSERS_EMEA = "adUsersGenerated_EMEA.csv"

# root forest
#New-ADOrganizationalUnit -Name "ForestUsers" -Path "DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
#New-ADOrganizationalUnit -Name "ForestServers" -Path "DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

# domain EMEA
New-ADOrganizationalUnit -Name "DomainUsers" -Path "DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "DomainServers" -Path "DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "DomainDesktops" -Path "DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

New-ADOrganizationalUnit -Name "ES- Alicante" -Path "OU=DomainDesktops,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Building A" -Path "OU=ES- Alicante,OU=DomainDesktops,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Building B" -Path "OU=ES- Alicante,OU=DomainDesktops,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "ES- Madrid" -Path "OU=DomainDesktops,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "FR- Paris" -Path "OU=DomainDesktops,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Republique" -Path "OU=FR- Paris,OU=DomainDesktops,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Montmartre" -Path "OU=FR- Paris,OU=DomainDesktops,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "FR- Versailles" -Path "OU=DomainDesktops,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "IT- Milan" -Path "OU=DomainDesktops,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "IT- Roma" -Path "OU=DomainDesktops,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "UK- London" -Path "OU=DomainDesktops,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

New-ADOrganizationalUnit -Name "DE- Frankfurt" -Path "OU=DomainServers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "FR- StDenis" -Path "OU=DomainServers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Room 1" -Path "OU=FR- StDenis,OU=DomainServers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Room 2" -Path "OU=FR- StDenis,OU=DomainServers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "IT- Tivoli" -Path "OU=DomainServers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "UK- Cambridge" -Path "OU=DomainServers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

New-ADOrganizationalUnit -Name "Others" -Path "OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "DE- Frankfurt" -Path "OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "PreSales" -Path "OU=DE- Frankfurt,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Customer Service" -Path "OU=DE- Frankfurt,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "DE- Berlin" -Path "OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Global Services" -Path "OU=DE- Berlin,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "NO- Tromso" -Path "OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Training" -Path "OU=NO- Tromso,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Energetics" -Path "OU=NO- Tromso,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CZ- Prague" -Path "OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Logistics" -Path "OU=CZ- Prague,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "ES- Alicante" -Path "OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Marketting" -Path "OU=ES- Alicante,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "ES- Madrid" -Path "OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "DevOps" -Path "OU=ES- Madrid,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Financial" -Path "OU=ES- Madrid,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "HR" -Path "OU=ES- Madrid,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Marketting" -Path "OU=ES- Madrid,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "FR- Paris" -Path "OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "DevOps" -Path "OU=FR- Paris,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "IT Helpdesk" -Path "OU=FR- Paris,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Marketting" -Path "OU=FR- Paris,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "FR- Versailles" -Path "OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "DSI" -Path "OU=FR- Versailles,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Financial" -Path "OU=FR- Versailles,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "HR" -Path "OU=FR- Versailles,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "IT- Roma" -Path "OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Financial" -Path "OU=IT- Roma,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "HR" -Path "OU=IT- Roma,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "IT- Milan" -Path "OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Marketting" -Path "OU=IT- Milan,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Q&A" -Path "OU=IT- Milan,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "UK- London" -Path "OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Financial" -Path "OU=UK- London,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "HR" -Path "OU=UK- London,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Marketting" -Path "OU=UK- London,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Legal" -Path "OU=UK- London,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "Validation" -Path "OU=UK- London,OU=DomainUsers,DC=emea,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

# arrays of sites by country
$sitesDE =@("OU=PreSales,OU=DE- Frankfurt,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=Customer Service,OU=DE- Frankfurt,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=Global Services,OU=DE- Berlin,OU=DomainUsers,DC=emea,DC=corp,DC=local"
		)
$sitesES =@("OU=Marketting,OU=ES- Alicante,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=DevOps,OU=ES- Madrid,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=Financial,OU=ES- Madrid,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=HR,OU=ES- Madrid,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=Marketting,OU=ES- Madrid,OU=DomainUsers,DC=emea,DC=corp,DC=local"
		)
$sitesFR =@("OU=DevOps,OU=FR- Paris,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=IT Helpdesk,OU=FR- Paris,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=Marketting,OU=FR- Paris,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=DSI,OU=FR- Versailles,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=Financial,OU=FR- Versailles,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=HR,OU=FR- Versailles,OU=DomainUsers,DC=emea,DC=corp,DC=local"
		)
$sitesIT =@("OU=Financial,OU=IT- Roma,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=HR,OU=IT- Roma,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=Marketting,OU=IT- Milan,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=Q&A,OU=IT- Milan,OU=DomainUsers,DC=emea,DC=corp,DC=local"
		)
$sitesUK =@("OU=Financial,OU=UK- London,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=HR,OU=UK- London,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=Marketting,OU=UK- London,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=Legal,OU=UK- London,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=Validation,OU=UK- London,OU=DomainUsers,DC=emea,DC=corp,DC=local"
		)
$sitesNO =@("OU=Energetics,OU=NO- Tromso,OU=DomainUsers,DC=emea,DC=corp,DC=local",
		"OU=Training,OU=NO- Tromso,OU=DomainUsers,DC=emea,DC=corp,DC=local"
		)
$sitesCZ = "OU=Logistics,OU=CZ- Prague,OU=DomainUsers,DC=emea,DC=corp,DC=local"
$siteDefault = "OU=Others,OU=DomainUsers,DC=emea,DC=corp,DC=local"

$sitesAll = $sitesDE + $sitesES + $sitesFR + $sitesIT + $sitesUK + $sitesNO + $sitesCZ

# --------------------------------
# create all users
$null=Import-Csv $CSV_RNDUSERS_EMEA -OutVariable usersEmea
ForEach($userEmea in $usersEmea){
# get the country 2-letter code
if($userEmea.CountryFull -like "Germany"){
	$ouPath = $sitesDE[(Get-Random -Minimum 0 -Maximum $sitesDE.Count)]
	$country = "DE"
}
elseif($userEmea.CountryFull -like "United Kingdom"){
	$ouPath = $sitesUK[(Get-Random -Minimum 0 -Maximum $sitesUK.Count)]
	$country = "GB"
}
elseif($userEmea.CountryFull -like "France"){
	$ouPath = $sitesFR[(Get-Random -Minimum 0 -Maximum $sitesFR.Count)]
	$country = "FR"
}
elseif($userEmea.CountryFull -like "Spain"){
	$ouPath = $sitesES[(Get-Random -Minimum 0 -Maximum $sitesES.Count)]
	$country = "ES"
}
elseif($userEmea.CountryFull -like "Italy"){
	$ouPath = $sitesIT[(Get-Random -Minimum 0 -Maximum $sitesIT.Count)]
	$country = "IT"
}
elseif($userEmea.CountryFull -like "Czech Republic"){
	#$ouPath = $sitesCZ[(Get-Random -Minimum 0 -Maximum $sitesCZ.Count)]
	$ouPath = $sitesCZ
	$country = "CZ"
}
elseif($userEmea.CountryFull -like "Norway"){
	$ouPath = $sitesNO[(Get-Random -Minimum 0 -Maximum $sitesNO.Count)]
	$country = "NO"
}
else{
	$ouPath = $siteDefault;
	$country = "FR"
}

# get the correct upn
$surnameNoAccents = [Text.Encoding]::ASCII.GetString([Text.Encoding]::GetEncoding(1251).GetBytes($userEmea.Surname))
$pattern = '[^a-zA-Z]'
$surnameNoSpecials = $surnameNoAccents -replace  $pattern,""
if(($surnameNoSpecials.Length) -lt 18){
	$upnBase = "$($userEmea.GivenName.Substring(0,1).ToLower())$($surnameNoSpecials.ToLower())"
}
else{
	$upnBase = "$($userEmea.GivenName.Substring(0,1).ToLower())$($surnameNoSpecials.Substring(0,18).ToLower())"
}
# try to find if exists
$counterUpn = 1
$upnBaseCorrected = $upnBase
$continueLoop = $true
while($continueLoop){
	try{
		$null=Get-ADUser -Identity $upnBaseCorrected
	}
	catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] {
		break
	}
	$upnBaseCorrected = $upnBase+$counterUpn
	$counterUpn += 1
}
# infos
Write-Host "Will create user '$upnBaseCorrected' in country $country and OU path '$ouPath'"
# create the user
New-ADUser -Name "$($userEmea.GivenName) $($userEmea.Surname)" -GivenName $userEmea.GivenName -Surname $userEmea.Surname`
			-DisplayName "$($userEmea.GivenName) $($userEmea.Surname)" `
			-AccountPassword $genericPass -CannotChangePassword $false -ChangePasswordAtLogon $false -PasswordNeverExpires $true `
			-StreetAddress $userEmea.StreetAddress -PostalCode $userEmea.ZipCode -City $userEmea.City -Country $country `
			-MobilePhone $userEmea.TelephoneNumber -Company $userEmea.Company `
			-EmailAddress "$upnBaseCorrected@emea.corp.com" -Organization "Corp" `
			-SamAccountName $upnBaseCorrected -UserPrincipalName "$upnBaseCorrected@emea.corp.local" -Path $ouPath -Enabled $true
}

# --------------------------------
# create all the groups
foreach($site in $sitesAll){
# get all users
$usersOfOU = Get-ADUser -SearchBase $site -Filter *
# generate a part of groups names and count
$numberOfGroups = Get-Random -Minimum 4 -Maximum 29
$groupBase = ($site -replace '^OU=|,.*$','')
$groupCountry = ($site -replace '^OU=.*?,OU=|,.*$','')
$groupNamed = "G_SG__"+ ($groupBase -replace '\s','-') +'_'+ ($groupCountry -replace '\s','')
# get group ou
$groupPath = $site -replace "^OU=$groupBase,",''
# for each group that might be created, create
1..$numberOfGroups | %{
	$indexesOfUsersCandidate = Get-Random -Minimum 0 -Maximum $usersOfOU.Count
	$group = $groupNamed +"__"+ $_
	# populate future group users
	$usersCandidateList = New-Object System.Collections.Generic.List[System.Object]
	1..$indexesOfUsersCandidate | %{ $usersCandidateList.Add($usersOfOU[$_]) }
	$usersCandidate = $usersCandidateList.ToArray()
	# when we have the candidates, create the group
	$generatedGroup = New-ADGroup -Name $group -DisplayName $group -SamAccountName $group `
						-GroupCategory "Security" -GroupScope "Global" `
						-Path $groupPath -PassThru
	# and push them to it
	$generatedGroup | Add-ADGroupMember -Members $usersCandidate
}
}