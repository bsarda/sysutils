# csv input, if not provided
$CSV_RNDUSERS_APJ  = "adUsersGenerated_APJ.csv"


# generic password for all users
$genericPass = ConvertTo-SecureString -AsPlainText -Force -String 'VMware1!'

# domain APJ
New-ADOrganizationalUnit -Name "DomainUsers" -Path "DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "DomainServers" -Path "DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "DomainDesktops" -Path "DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

New-ADOrganizationalUnit -Name "JP- Tokyo" -Path "OU=DomainDesktops,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Site North" -Path "OU=JP- Tokyo,OU=DomainDesktops,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Site South" -Path "OU=JP- Tokyo,OU=DomainDesktops,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "JP- Sapporo" -Path "OU=DomainDesktops,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CN- Shangai" -Path "OU=DomainDesktops,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Qingpu" -Path "OU=CN- Shangai,OU=DomainDesktops,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Fengxian" -Path "OU=CN- Shangai,OU=DomainDesktops,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CN- Beijing" -Path "OU=DomainDesktops,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CN- Chengdu" -Path "OU=DomainDesktops,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "KR- Seoul" -Path "OU=DomainDesktops,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "MY- Singapore" -Path "OU=DomainDesktops,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

New-ADOrganizationalUnit -Name "JP- Yokohama" -Path "OU=DomainServers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CN- Honk Kong" -Path "OU=DomainServers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Room 1" -Path "OU=CN- Honk Kong,OU=DomainServers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Room 2" -Path "OU=CN- Honk Kong,OU=DomainServers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "KR- Seoul" -Path "OU=DomainServers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "MY- Kuala Lumpur" -Path "OU=DomainServers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

New-ADOrganizationalUnit -Name "Others" -Path "OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "JP- Yokohama" -Path "OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "DevOps" -Path "OU=JP- Yokohama,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "JP- Tokyo" -Path "OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Marketting" -Path "OU=JP- Tokyo,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "JP- Sapporo" -Path "OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Financial" -Path "OU=JP- Sapporo,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "HR" -Path "OU=JP- Sapporo,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CN- Shangai" -Path "OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "DevOps" -Path "OU=CN- Shangai,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "HR" -Path "OU=CN- Shangai,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Marketting" -Path "OU=CN- Shangai,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CN- Beijing" -Path "OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "DSI" -Path "OU=CN- Beijing,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Financial" -Path "OU=CN- Beijing,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "CN- Chengdu" -Path "OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "IT Helpdesk" -Path "OU=CN- Chengdu,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "KR- Seoul" -Path "OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Financial" -Path "OU=KR- Seoul,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "HR" -Path "OU=KR- Seoul,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Marketting" -Path "OU=KR- Seoul,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Q&A" -Path "OU=KR- Seoul,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
New-ADOrganizationalUnit -Name "MY- Singapore" -Path "OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Financial" -Path "OU=MY- Singapore,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "HR" -Path "OU=MY- Singapore,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Marketting" -Path "OU=MY- Singapore,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Legal" -Path "OU=MY- Singapore,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false
	New-ADOrganizationalUnit -Name "Validation" -Path "OU=MY- Singapore,OU=DomainUsers,DC=apj,DC=corp,DC=local" -ProtectedFromAccidentalDeletion $false

# arrays of sites by country
$sitesJP =@("OU=DevOps,OU=JP- Yokohama,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=Marketting,OU=JP- Tokyo,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=Financial,OU=JP- Sapporo,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=HR,OU=JP- Sapporo,OU=DomainUsers,DC=apj,DC=corp,DC=local"
			)
$sitesCN =@("OU=DevOps,OU=CN- Shangai,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=HR,OU=CN- Shangai,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=Marketting,OU=CN- Shangai,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=DSI,OU=CN- Beijing,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=Financial,OU=CN- Beijing,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=IT Helpdesk,OU=CN- Chengdu,OU=DomainUsers,DC=apj,DC=corp,DC=local"
			)
$sitesKR =@("OU=Financial,OU=KR- Seoul,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=HR,OU=KR- Seoul,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=Marketting,OU=KR- Seoul,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=Q&A,OU=KR- Seoul,OU=DomainUsers,DC=apj,DC=corp,DC=local"
			)
$sitesMY =@("OU=Financial,OU=MY- Singapore,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=HR,OU=MY- Singapore,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=Marketting,OU=MY- Singapore,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=Legal,OU=MY- Singapore,OU=DomainUsers,DC=apj,DC=corp,DC=local",
			"OU=Validation,OU=MY- Singapore,OU=DomainUsers,DC=apj,DC=corp,DC=local"
			)
$siteDefault = "OU=Others,OU=DomainUsers,DC=apj,DC=corp,DC=local"

$sitesAll = $sitesJP + $sitesCN + $sitesKR + $sitesMY

# --------------------------------
# create all users
$null=Import-Csv $CSV_RNDUSERS_APJ -OutVariable usersApj
ForEach($userApj in $usersApj){
	$rnd = Get-Random -Minimum 1 -Maximum 100
	# get the country 2-letter code
	if($rnd -lt 30){
		$ouPath = $sitesJP[(Get-Random -Minimum 0 -Maximum $sitesJP.Count)]
		$country = "JP"
	}
	elseif(($rnd -lt 70) -and ($rnd -ge 30)){
		$ouPath = $sitesCN[(Get-Random -Minimum 0 -Maximum $sitesCN.Count)]
		$country = "CN"
	}
	elseif(($rnd -lt 85) -and ($rnd -ge 70)){
		$ouPath = $sitesKR[(Get-Random -Minimum 0 -Maximum $sitesKR.Count)]
		$country = "KR"
	}
	elseif($rnd -ge 85){
		$ouPath = $sitesMY[(Get-Random -Minimum 0 -Maximum $sitesMY.Count)]
		$country = "MY"
	}
	else{
		$ouPath = $siteDefault;
		$country = "FR"
	}
	
	# get the correct upn
	#$surnameNoAccents = [Text.Encoding]::ASCII.GetString([Text.Encoding]::GetEncoding(1251).GetBytes($userApj.Surname))
	#$pattern = '[^a-zA-Z]'
	#$surnameNoSpecials = $surnameNoAccents -replace  $pattern,""
	#if(($surnameNoSpecials.Length) -lt 18){
	#    $upnBase = "$($userApj.GivenName.Substring(0,1).ToLower())$($surnameNoSpecials.ToLower())"
	#}
	#else{
	#    $upnBase = "$($userApj.GivenName.Substring(0,1).ToLower())$($surnameNoSpecials.Substring(0,18).ToLower())"
	#}	
	if(($userApj.Surname.Length) -lt 18){
		$upnBase = "$($userApj.GivenName.Substring(0,1).ToLower())$($userApj.Surname.ToLower())"
	}
	else{
		$upnBase = "$($userApj.GivenName.Substring(0,1).ToLower())$($userApj.Surname.Substring(0,18).ToLower())"
	}
	
	# try to find if exists
	$counterUpn = 1
	$upnBaseCorrected = $upnBase
	$continueLoop = $true
	while($continueLoop){
		try{
			$null=Get-ADUser -Identity $upnBaseCorrected
		}
		catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] {
			break
		}
		$upnBaseCorrected = $upnBase+$counterUpn
		$counterUpn += 1
	}
	# infos
	Write-Host "Will create user '$upnBaseCorrected' in country $country and OU path '$ouPath'"

	# create the user
	New-ADUser -Name "$($userApj.GivenName) $($userApj.Surname)" -GivenName $userApj.GivenName -Surname $userApj.Surname`
				-DisplayName "$($userApj.GivenName) $($userApj.Surname)" `
				-AccountPassword $genericPass -CannotChangePassword $false -ChangePasswordAtLogon $false -PasswordNeverExpires $true `
				-Country $country `
				-MobilePhone ((1..9 | % {Get-Random -Minimum 0 -Maximum 9}) -join $_) -Company $userApj.Company `
				-EmailAddress "$upnBaseCorrected@apj.corp.com" -Organization "Corp" `
				-SamAccountName $upnBaseCorrected -UserPrincipalName "$upnBaseCorrected@apj.corp.local" -Path $ouPath -Enabled $true
}

# --------------------------------
# create all the groups
foreach($site in $sitesAll){
	# get all users
	$usersOfOU = Get-ADUser -SearchBase $site -Filter *
	# generate a part of groups names and count
	$numberOfGroups = Get-Random -Minimum 4 -Maximum 29
	$groupBase = ($site -replace '^OU=|,.*$','')
	$groupCountry = ($site -replace '^OU=.*?,OU=|,.*$','')
	$groupNamed = "G_SG__"+ ($groupBase -replace '\s','-') +'_'+ ($groupCountry -replace '\s','')
	# get group ou
	$groupPath = $site -replace "^OU=$groupBase,",''
	# for each group that might be created, create
	1..$numberOfGroups | %{
		$indexesOfUsersCandidate = Get-Random -Minimum 0 -Maximum $usersOfOU.Count
		$group = $groupNamed +"__"+ $_
		# populate future group users
		$usersCandidateList = New-Object System.Collections.Generic.List[System.Object]
		1..$indexesOfUsersCandidate | %{ $usersCandidateList.Add($usersOfOU[$_]) }
		$usersCandidate = $usersCandidateList.ToArray()
		# when we have the candidates, create the group
		$generatedGroup = New-ADGroup -Name $group -DisplayName $group -SamAccountName $group `
							-GroupCategory "Security" -GroupScope "Global" `
							-Path $groupPath -PassThru
		# and push them to it
		$generatedGroup | Add-ADGroupMember -Members $usersCandidate
	}
}
