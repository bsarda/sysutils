#!/bin/bash
#
# mount the remote :
# sudo sshfs -o allow_other root@192.168.1.202:/var/media /media/bsarda/202
#
echo "Searching from root folder: $1"
echo "=================================================="
echo "Files that are NOT in the correct format:"
  filelistnot=$(find $1 -type f \( -not -name "*.mkv" -not -name "*.avi" -not -name "*.mp4" \) )
  echo "$filelistnot"
echo "\n\n"
echo "=================================================="
echo "Files that are NOT in fullHD (width<1800)"

find $1 -type f \( -name "*.mkv" -o -name "*.avi" -o -name "*.mp4" \) -exec bash -c '
  for filepath do
    width=$(exiftool "$filepath" -fast | grep "Image Width" | sed s/.*:// | tr -d " ")
    if (( $width < 1800 ))
    then
      echo "width = $width ;; path => $filepath"
    fi
  done' bash {} +

