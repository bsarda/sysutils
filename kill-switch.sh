#!/bin/bash
for (( ; ; ))
do
  vpnps=$(pidof openvpn)
  isnordvpn=$(ps aux | grep [n]ordvpn.com)
  istun=$(ip addr | grep tun)
  if [ -z "$istun" -o -z "$vpnps" -o -z "$isnordvpn" ]; then
    #echo "NO TUNNEL !!!"
    pkill transmission-*
  fi
  sleep 0.5
done

