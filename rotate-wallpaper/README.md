# Automatic rotate wallpapers

## Intro

This is a small bash script to rotate wallpapers/backgrounds.
By defaut this is every 5 minutes, and locations are:  
~/Pictures/Wallpapers-morning ; ~/Pictures/Wallpapers-noon ; ~/Pictures/Wallpapers-afternoon ; ~/Pictures/Wallpapers-night

## flavor and files here

There is the 2 flavors: ubuntu or debian.  
When copying, remove the -debian or -ubuntu suffix.

Files are:

- `rotate-background-cron-debian.sh` = the script for option 1, working on debian 10
- `rotate-background-cron-ubuntu.sh` = the script for option 1, working on ubuntu 19.10
- `rotate-backgrounds-option2.sh` and `launch-backgrounds` = the script for option 2, working on debian 10

## Basics

Copy the .sh files to /opt/  
Change the owner to be the current user, and add the execution permission.

## Option1 (preferred) : Cron it

:warning: This is the preferred option

```console
crontab -e
```

add a line to cron it every 10min (for example)

```console
*/10 * * * * /opt/rotate-background.sh
```

## Option2 : Launch at start

Add to the ~/.profile :

```javascript
echo "/opt/launch-backgrounds.sh" >> ~/.profile
```

