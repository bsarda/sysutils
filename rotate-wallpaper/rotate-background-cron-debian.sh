#!/bin/sh
WALLPAPER_DIR_MORNING=~/Pictures/Wallpapers-morning
WALLPAPER_DIR_NOON=~/Pictures/Wallpapers-noon
WALLPAPER_DIR_AFTERNOON=~/Pictures/Wallpapers-afternoon
WALLPAPER_DIR_NIGHT=~/Pictures/Wallpapers-night

# get current hour of the day
hour=$(date +%H)

# get the dbus session address of gnome to be able to change graphic for cron-ed user
PID=$(pgrep gnome-session) 
export DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$PID/environ|cut -d= -f2-) 

# depending on the hour of the day...
case $hour in
  0|1|2|3|4|5|6|7|00|01|02|03|04|05|06|07|21|22|23)
    image=`ls $WALLPAPER_DIR_NIGHT | shuf -n 1`
    if [ ! -z "$image" ]
    then
      gsettings set org.gnome.desktop.background picture-uri "file://$WALLPAPER_DIR_NIGHT/$image"
    fi
    ;;
  8|9|10|11|08|09)
    image=`ls $WALLPAPER_DIR_MORNING | shuf -n 1`
    if [ ! -z "$image" ]
    then
      gsettings set org.gnome.desktop.background picture-uri "file://$WALLPAPER_DIR_MORNING/$image"
    fi
    ;;
  12|13)
    image=`ls $WALLPAPER_DIR_NOON | shuf -n 1`
    if [ ! -z "$image" ]
    then
      gsettings set org.gnome.desktop.background picture-uri "file://$WALLPAPER_DIR_NOON/$image"
    fi
    ;;
  14|15|16|17|18|19|20)
    image=`ls $WALLPAPER_DIR_AFTERNOON | shuf -n 1`
    if [ ! -z "$image" ]
    then
      gsettings set org.gnome.desktop.background picture-uri "file://$WALLPAPER_DIR_AFTERNOON/$image"
    fi
    ;;
  *)
esac

# exit with nil
return 0

