#!/bin/sh
WALLPAPER_DIR_MORNING=~/Pictures/Wallpapers-morning
WALLPAPER_DIR_NOON=~/Pictures/Wallpapers-noon
WALLPAPER_DIR_AFTERNOON=~/Pictures/Wallpapers-afternoon
WALLPAPER_DIR_NIGHT=~/Pictures/Wallpapers-night

while true
do
  hour=$(date +%H)
  case $hour in
    0|1|2|3|4|5|6|7|21|22|23)
      image=`ls $WALLPAPER_DIR_NIGHT | shuf -n 1`
      if [ ! -z "$image" ]
      then
        echo "Changing to image $image"
        gsettings set org.gnome.desktop.background picture-uri "file://$WALLPAPER_DIR_NIGHT/$image"
      fi
      ;;
    8|9|10|11)
      image=`ls $WALLPAPER_DIR_MORNING | shuf -n 1`
      if [ ! -z "$image" ]
      then
        echo "Changing to image $image"
        gsettings set org.gnome.desktop.background picture-uri "file://$WALLPAPER_DIR_MORNING/$image"
      fi
      ;;
    12|13)
      image=`ls $WALLPAPER_DIR_NOON | shuf -n 1`
      if [ ! -z "$image" ]
      then
        echo "Changing to image $image"
        gsettings set org.gnome.desktop.background picture-uri "file://$WALLPAPER_DIR_NOON/$image"
      fi
      ;;
    14|15|16|17|18|19|20)
      image=`ls $WALLPAPER_DIR_AFTERNOON | shuf -n 1`
      if [ ! -z "$image" ]
      then
        echo "Changing to image $image"
        gsettings set org.gnome.desktop.background picture-uri "file://$WALLPAPER_DIR_AFTERNOON/$image"
      fi
      ;;
    *)
   esac
   sleep 300
done

