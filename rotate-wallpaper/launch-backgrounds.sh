#!/bin/bash
if [ ! -f /tmp/rotate-backgrounds.pid ]; then
  PROGRAM=/opt/rotate-backgrounds-option2.sh
  echo $PROGRAM > /tmp/rotate-backgrounds.error
  nohup $PROGRAM 2>/tmp/rotate-backgrounds.error 1>/dev/null &
  PID=$!
  echo $PID > /tmp/rotate-backgrounds.pid
fi

exit 0
