#!/bin/sh
# requirement : install wallpaper util : brew install wallpaper

# func to get random file from folder
get_rand_file() {
  files=( $1/* )
  num=${#files[@]}
  rand_idx=$(( RANDOM % num ))
  rand_file=${files[rand_idx]}
  echo "$rand_file"
}

# directories where to look for images
WALLPAPER_DIR_MORNING=~/Pictures/Wallpapers-morning
WALLPAPER_DIR_NOON=~/Pictures/Wallpapers-noon
WALLPAPER_DIR_AFTERNOON=~/Pictures/Wallpapers-afternoon
WALLPAPER_DIR_NIGHT=~/Pictures/Wallpapers-night
WALLPAPER_DIR_EVENING=~/Pictures/Wallpapers-evening

# get current hour of the day
hour=$(date +%H)
case $hour in
  0|1|2|3|4|5|6|7|00|01|02|03|04|05|06|07|22|23)
    image=$(get_rand_file $WALLPAPER_DIR_NIGHT)
    ;;
  8|9|10|11|08|09)
    image=$(get_rand_file $WALLPAPER_DIR_MORNING)
    ;;
  12|13)
    image=$(get_rand_file $WALLPAPER_DIR_NOON)
    ;;
  18|19|20|21)
    image=$(get_rand_file $WALLPAPER_DIR_EVENING)
    ;;
  14|15|16|17|18|19|20)
    image=$(get_rand_file $WALLPAPER_DIR_AFTERNOON)
    ;;
  *)
esac
# set the wallpaper
if [ ! -z "$image" ]
then
  /usr/local/bin/wallpaper set "$image"
fi

# exit with nil
# return 0
