#!/bin/sh
# exiftool_set_create_date: set the file date from exif "Creation Date".
# usage : find ./test/ -type f -exec ./exiftool_set_create_date.sh '{}' ';'
echo -n "$@:  "

# This script takes one filename as option.
# Escape space in filenames.
# Other special chracters might be handled here, too.
FILE="$(
	echo $@ | 
	sed "s| |\\ |g"
)"

# Get the date from the exif data.
DATE="$(
	exiftool -ee -CreateDate "$FILE" |
	grep -m1 -Eo "[[0-9]{1,4}:]?[0-9]{2}:[0-9]{2} [0-9]{2}:[0-9]{2}:[[0-9]{2}]?" |
	sed "s|\:||g" |
        sed "s| ||g"
)"

# Convert it for "touch". Expected format is [[CC]YY]MMDDhhmm[.ss]
[ -n "$DATE" ] &&
	if [ "$( echo "$DATE" | wc -m )" -eq 15 ] ; then
		DATE_TOUCH="$( echo "$DATE" | grep -Eo "^[0-9]{12}").$( echo "$DATE" | grep -Eo "[0-9]{2}$" )"
	elif [ "$( echo "$DATE" | wc -m )" -ge 9 ] && [ "$( echo "$DATE" | wc -m )" -le 15 ] ; then
		DATE_TOUCH=$DATE
	fi

# Set the date on the file.
[ -n "$DATE_TOUCH" ] &&
	echo -n " touching with $DATE_TOUCH" &&
	touch -t $DATE_TOUCH "$FILE" &&
	echo " ...done." &&
	echo "$FILE" >> ~/"$(basename $0)".log ||
	{
	echo " ...failed."
	echo "$FILE" >> ~/"$(basename $0)"_fail.log
	}

exit
